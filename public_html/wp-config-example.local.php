<?php
/**
 * The environment configurations of the WordPress.
 *
 * This file has the following local configurations: MySQL settings, Table Prefix
 * and the environmental settings
 *
 * This file is included by wp-config.php
 */

define( 'WP_ENV', 'development' ); // development/staging/production

define( 'DB_NAME', 'Habeon.HabeonWebsite' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );

define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );
define( 'WP_DEBUG_LOG', true );

define( 'WP_INDEX', false );


define( 'WP_PROTO', 'http' );
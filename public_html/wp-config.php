<?php


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

require_once( 'wp-config.local.php' );

if(!defined("WP_PROTO")){
	define( 'WP_PROTO', "https");
}

define('WP_CACHE', true); //Added by WP-Cache Manager

define( 'WP_SITEURL', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] . '/wp' );
define( 'WP_HOME', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] );

define( 'WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content' );
define( 'WP_CONTENT_URL', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] . '/wp-content' );

define( 'WP_PLUGIN_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins' );
define( 'WP_PLUGIN_URL', WP_HOME. '/wp-content/plugins' );

define( 'WPCACHEHOME', WP_CONTENT_DIR.'/plugins/wp-super-cache/' ); //Added by WP-Cache Manager


define( 'EMPTY_TRASH_DAYS', 300 );
define( 'WP_POST_REVISIONS', 250 );

// define( 'WP_DEFAULT_THEME', 'src' );


define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);


define("GOOGLE_ANALYTICS_ID","");

if(!defined("VERSION")){
	define("VERSION",time());
}


if(!defined("IMGFIX")){
	define("IMGFIX",true);
}

if(!defined("CURRENTSEASON")){
	


	$currentMonth=DATE("m");

	//retrieve season
	if ($currentMonth === "03" or $currentMonth === "04")
		$currentSeason = "spring";
	elseif ($currentMonth === "07" or $currentMonth === "08")
		$currentSeason = "summer";
	elseif ($currentMonth === "09" or $currentMonth === "10" or $currentMonth === "11")
		$currentSeason = "autumn";
	elseif ($currentMonth === "05" or $currentMonth === "06")
		$currentSeason = "presummer";
	else
		$currentSeason = "winter";
	

	define("CURRENTSEASON",$currentSeason);


	
}

define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7I$Fy};6hh-T.<%2`W.ET.0=_)Wlg+9xd:}$f9R~t7R8n;([R/*v%3AMXK@QN%WD');
define('SECURE_AUTH_KEY',  '1)xMuCu6|MSysuoGqntb{-N>[Se7V+<*8X  8_h5`-h^{R^KKvfuo{a+SZf`88OY');
define('LOGGED_IN_KEY',    '|E,{Mz-##13;$[D,Bq9Esk#23*r(||wwy;#J?nW0zqu9@t%%g8[U=)d&,)[N;):a');
define('NONCE_KEY',        'q.||CMg-%(G5LW)MTwr]A=BM5<R=WwPthD={.KV+LSCA|KMHNL|>-qK_(lcO5+X5');
define('AUTH_SALT',        'JcT[8e7FA:TA_gx0O+|qP|+!YGF5rO^U]h6(9#zcU`FxK-wjuj,y)xoPdF(aw]Wt');
define('SECURE_AUTH_SALT', 'N9b{?0envxy2Yr=+TA<TQGV-]MC^SF#A(_&|j+St,7BGjRV1TLeUysQVj0sN_aCq');
define('LOGGED_IN_SALT',   'JE t o~v`f$X4((k,(F:I#oE1])P:RX_+x+L(^wSp>)<5;hCk-Q%^7ad{^if2w-s');
define('NONCE_SALT',       '@XWdjcFW2ZJ%{marVKQR#2|8h+ZNDfdXK:[)<f|}wUX*hv`C:ww8+L~V$<`42tdE');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'habeon_wp_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/**
 * Disable search engines when not on production server
 */
if ( !WP_INDEX ) { add_filter( 'pre_option_blog_public', '__return_zero' ); } // Privacy, robots no-follow metatag

/**
 * Fix chmod
 * file acces read/write for ftp
 */
if( is_admin() ) { add_filter( 'filesystem_method', create_function('$a', 'return "direct";' ) ); define( 'FS_CHMOD_DIR', 0751 ); }
<?php
/**
 * The template for displaying single products.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Rungis
 */

//$introTitle = get_field('product_intro_title');
$introTitle = get_the_title();
$introText = get_field('product_intro_text');

$mapTitle = get_field('product_map_title');
$mapMap = get_field('product_map');

$moreinfo = get_field('product_meerinformatie');
if(!IMGFIX){
	$gallery = get_field('product_slider');	
}else{
	$gallery = getimagesforproduct();
}


$landOfOrigin = get_field("land_of_origin");


$productNumber = get_field("reference");

$bestelbaarZakelijk = get_field("bestelbaar_zakelijk");
$bestelbaarParticulier = get_field("bestelbaar_particulier");



if($bestelbaarParticulier === null){
	$bestelbaarParticulier = true;
}

if($bestelbaarZakelijk === null){
	$bestelbaarZakelijk = true;
}









get_header(); ?>

<div class="wrap pd">

	<div class="cms-content is-mobile">
		<h1><?php echo $introTitle; ?></h1>
		<h4><?php echo __("Artikelnummer");?>: <?php echo $productNumber; ?></h4>
		<div class="tags">
		<?php
			$ts = array();
			if(get_field("biologisch")){
				$ts[] = __("Biologisch","rungis");
			}

			$terms = get_the_terms( null, "productcategory" );
			foreach($terms as $term){
				$ts[] = $term->name;
			}

			
			foreach($ts as $t){
				?>
				<span class="tag"><?php echo $t;?></span>
				<?php
			}
		?>
			
			
		</div>
		
	</div>
	<div class="row">
		<div class="col col-40">
			<div class="pd__slider">
				<div id="slider" class="slider">					
					<?php 
						if($gallery):
							
							foreach($gallery as $item){

								if(!IMGFIX){


									if(is_numeric($item)){
										$item2 = basename(get_attached_file($item));
									}else{
										$item2 = $item['filename'];
									}
									echo "<!-- ";
									print_r($item2);
									echo "-->";
									if(!preg_match("/".$productNumber."_\d+/", $item2)){
										continue;
									}
								}

								echo '<div class="slide">';
								hm_get_template_part('parts/image', array('image'=>($item),'size'=>'large')); 
								echo '</div>';
							};
						endif;
					?>					
				</div>
				
				<div id="thumbnails" class="thumbs">
					<?php 
						if($gallery):
							foreach($gallery as $item){
								if(!IMGFIX){

									if(is_numeric($item)){
										$item2 = basename(get_attached_file($item));
									}else{
										$item2 = $item['filename'];
									}
									echo "<!-- ";
									print_r($item2);
									echo "-->";
									if(!preg_match("/".$productNumber."_\d+/", $item2)){
										continue;
									}
								}
								
								echo '<button class="thumbs__item">';
								hm_get_template_part('parts/image', array('image'=>($item),'size'=>'large')); 
								echo '</button>';
							};
						endif;
					?>					
				</div>
			</div>
		</div>
		<div class="col col-60">
			<div class="tags is-desktop">
			<?php
				$ts = array();
				if(get_field("biologisch")){
					$ts[] = __("Biologisch","rungis");
				}

				$terms = get_the_terms( null, "productcategory" );
				foreach($terms as $term){
					$ts[] = $term->name;
				}

				
				foreach($ts as $t){
					?>
					<span class="tag"><?php echo $t;?></span>
					<?php
				}
			?>
				
				
			</div>
			<style>
				.order--large .options .subtrigger.business{
					
					float:left;
				}

				<?php if(!$bestelbaarZakelijk){ ?>
					.order--large .options .subtrigger.private{
						float:left;
					}
				<?php } ?>
			</style>
			<div class="pd__intro">
				<div class="cms-content ">
					<h1 class='is-desktop'><?php echo $introTitle; ?></h1>
					<h4 class='is-desktop'><?php echo __("Artikelnummer");?>: <?php echo $productNumber; ?></h4>
					<?php echo $introText; ?>
					
				</div>
				<div class="pd__intro__footer">					
					<?php if(get_field("can_be_ordered") == "Yes") { ?>
					
					<div class="order order--large">
						<span class="trigger"><?php echo __("Bestellen","rungis");?></span>
						<div class="options">

							<?php if($bestelbaarParticulier){?>
							<a href="https://directlyfrom.nl/search?controller=search&orderby=position&orderway=desc&search_query=<?php echo $productNumber;?>&submit_search=" class="subtrigger private" target="_blank">
								<?php echo __("Bestel particulier","rungis");?>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
							</a>
							<?php } ?>



							<?php if($bestelbaarZakelijk){ ?>
							<span class="subtrigger business">
								<?php echo __("Bestel zakelijk","rungis");?>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
							</span>
							<div class="bestelbox">
								<a href="http://www.bestelbox.nl/__C1256ED3002A8D2C.nsf/agtCreateOrderByDiscountId?openagent&supplierid=8KLLFF&prodid=<?php echo $productNumber;?>" class="loginyes" target="_blank">
									<?php echo __("Ik heb een inONE Order login","rungis");?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
								</a>
								<a href="<?php echo get_url_for_language('/bestellen/');?>" class="loginno" target="_blank">
									<?php echo __("Ik heb geen inONE Order login","rungis");?>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
								</a>
							</div>
							<?php } ?>
						</div>
					</div>
					
					<?php } ?>					
				</div>
			</div>
			
			<?php hm_get_template_part('parts/socialshare');  ?>
		</div>
	</div>
	
	<hr/>
	
	<div id="moreinfo" class="row cms-content">
		<div class="col col-<?php if($moreinfo) { ?>40<?php } else { ?>100<?php } ?>">
			<h2>Herkomst</h2>
			<div id="map" class="map js-map" data-location="<?php echo $landOfOrigin;?>" data-lat="<?php echo $mapMap['lat']; ?>" data-lng="<?php echo $mapMap['lng']; ?>"></div>
		</div>
		
		<div class="col col-<?php if($moreinfo) { ?>60<?php } else { ?>100<?php } ?>">
			<div class="cms-content">
				<?php if($moreinfo) { ?><h2>Meer informatie</h2><?php } ?>
				<p>

				<?php echo $moreinfo; ?>
				</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
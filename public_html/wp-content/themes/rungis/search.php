<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package Rungis
 */

get_header(); ?>

<?php // hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo __("Zoekresultaten","rungis");?></h1>
		</div>
	</div>
	
	<div class="searchresult">
		<div class="cms-content">		
						
			
			<?php
			
			function showResults($post_type,$title){
				$first = false;

				rewind_posts();
				
				while ( have_posts() ) : the_post();

					if(get_post_type() == $post_type){
						
						if(!$first){ ?>
						<div class="searchresult__items">
							<p class="title"><?php echo $title;?></p>
							
						<?php 
						$first = true;
						}

						get_template_part("parts/searchresult");

					}
					

				endwhile;

				if($first){
					?>
					</div>
					<?php
				}
	
			}

			?>
			
			<?php if ( have_posts() ) : ?>
				<?php

					showResults("product",__("Producten","rungis"));
					showResults("post",__("Inspiratie","rungis"));
					showResults("page",__("Pagina's","rungis"));


					hm_get_template_part('parts/pagination'); 
					

				else :
					?>
					<h2>Helaas</h2>
					<p>
						Er zijn geen resultaten gevonden.
					</p>
					<?php
				endif; 
				wp_reset_postdata();
				wp_reset_query();
				
			?>
		

		</div>
	</div>
</div>

<?php
get_footer();
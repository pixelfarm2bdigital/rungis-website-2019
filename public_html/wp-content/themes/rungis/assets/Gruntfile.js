module.exports = function(grunt) {

	require('time-grunt')(grunt);
	require('load-grunt-tasks')(grunt); 

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass : {
			options : {
				sourceMap : false,
				includePaths : require('node-bourbon').with('node_modules/foundation-sites/scss/')

			},
			dev : {
				files : {
					'css/style.css' : 'scss/style.scss'
				},
				options: {
					outputStyle : "expanded"
				}
			},
			dist : {
				files : {
					'css/style.min.css' : 'scss/style.scss'
				},
				options: {
					outputStyle : "compressed"
				}
			}
		},
		
		watch : {
			php : {
				files : ['*.php', '../**/*.php', '../*.php']       
			},
			scss : {
				files : ['scss/*.scss', 'scss/**/*.scss'],
				tasks : ['sass:dev', 'postcss:dev'],

			},
			js_libs : {
				files : ['js/dev/plugins/*.js'],
				tasks : ['uglify:libs_dev']
			},
			js_custom : {
				files : ['js/dev/custom/*.js'],
				tasks : ['uglify:custom_dev']        
			},
			compiled_css : {
				files :['css/style.css']       
			},
			svgs : {
				files : 'img/svg/*.svg',
				tasks: ['build']
			},
			options : {
				livereload: true
			}
		},
		
		svgstore : {
  			options : {
  				cleanup : true,
  				prefix : 'icon-',
  				svg : {
  					id : 'svg-sprite',
  					class : 'svg-hide',
  					xmlns : 'http://www.w3.org/2000/svg'
  				}
  			},
  			build : {
  				files : {
  					'img/sprite-icons.svg' : ['img/svg/*.svg']
  				}
  			}
  		},

		postcss : {			
			dev: {
				src : 'css/style.css',
				options : {
					map : true,
					processors : [
						require('pixrem')(),    // add fallbacks form rem units
					]
				},
			},
			dist : {
				src : 'css/style.min.css',
				options : {
					map : false,
					processors : [
						require('pixrem')(),    // add fallbacks form rem units
						require('cssnano')()    // minify the result
					]
				},
			}
		},

		uglify : {
			custom_dev : {
				files : {
					'js/scripts.js' : ['js/dev/custom/*.js']
				},
				options: {
					beautify : true
				}
			},
			custom_dist : {
				files : {
					'js/scripts.min.js' : ['js/dev/custom/*.js']
				},
				options: {
					compress : true
				}
			},
			libs_dev : {
				files : {
					'js/libs.js' : ['js/dev/plugins/*.js'],
				},
				options: {
					beautify: true
				}
			},
			libs_dist : {
				files : {
					'js/libs.min.js' : ['js/dev/plugins/*.js']
				},
				options: {
					compress : true
				}
			}
		}
	});

	grunt.registerTask('default', [
		'dev'
	]);
	grunt.registerTask('dev', [
    	'sass:dev',
    	'postcss:dev',
        'uglify:custom_dev',
        'uglify:libs_dev',
        'watch',
    ]);
	
	grunt.registerTask('build', [
    	'sass:dist',
    	'postcss:dist',
        'uglify:custom_dist',
        'uglify:libs_dist',
    ]);
};
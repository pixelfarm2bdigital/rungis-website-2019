$(document).ready(function(){
	$(".media__filter").each(function(){
		var _ = $(this);
		var $all = $("#all");
		

		var $checkboxes = $("[name='media']");


		var base = document.location.pathname;
		_.find("#digitale,#vakbladen,#kranten").on("change",function(){
			
			/*
			if(($digitale[0].checked && $vakbladen[0].checked)){
				document.location = base;
			}else if($digitale[0].checked){
				document.location = base+"?type=digitale-media";
			}else if($vakbladen[0].checked){
				document.location = base+"?type=vakbladen";
			}else if($kranten[0].checked){
				document.location = base+"?type=kranten";
			}
			*/
			//get the new url
			var types = [];
			$checkboxes.each(function(){
				if(this.checked){
					types.push(this.value);
				}
			});

			document.location = base+"?type="+types.join(",");
		});

		
		$all.on('change',function(){
			if($all[0].checked){
				$checkboxes.prop("checked",true);
				$checkboxes.first().trigger("change");
			}else{
				$checkboxes.prop("checked",false);
			}
			

		})
		

		if($checkboxes.filter(":checked").length == $checkboxes.length){
			$all.prop("checked",true);
		}
		
	});
});
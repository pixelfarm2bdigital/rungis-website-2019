
window.googleMapsStyling = [
			{
				"featureType": "administrative", 
				"elementType": "labels.text.fill", 
				"stylers": [{
						"color": "#444444"
					} 
				]
			}, 
			{
			
				"featureType": "landscape", 
				"elementType": "all", 
				"stylers": [{
					"color": "#ffffff"
				}]
			}, 
			{
				"featureType": "poi", 
				"elementType": "all", 
				"stylers": [{
					"visibility": "off"
				}]
			},
			{
				"featureType": "road", 
				"elementType": "all", 
				"stylers": [{
					"saturation": -100
				}, {
					"lightness": 45 
				}] 
			},
			{
				"featureType": "road", 
				"elementType": "geometry.fill", 
				"stylers": [{
					"color": "#d9d9d9"
				}, {
					"saturation": "0" 
				}, {
					"gamma": "2.13"
				}]
			}, {
				"featureType": "road.highway", 
				"elementType": "all",
				"stylers": [{
					"visibility": "simplified" 
				}] 
			},
			{
				"featureType": "road.arterial", 
				"elementType": "labels.icon", 
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "transit", 
				"elementType": "all", 
				"stylers": [{
					"visibility": "off"
				}]
			}, {
				"featureType": "water", 
				"elementType": "all", 
				"stylers": [{
					"color": window.colorPrimary
				}, {
					"visibility": "on"
				}, {
					"lightness": "0"
				}, {
					"gamma": "1" 
				}, {
					"saturation": "0"
				}, {
					"weight": "0.83" 
				}
			] }
		];
function initMap() {
	
	var map = document.getElementById('map');
	
	var location = map.getAttribute("data-location");

	
	
	//console.log(rungis2);

	var markerFolder = '/wp-content/themes/rungis/assets/img/';
	var mapContainer = map;
	
		
	if(!location){
		var lat = map.getAttribute('data-lat');
		var lng = map.getAttribute('data-lng');
	
		var rungis = { lat: parseFloat(lat), lng: parseFloat(lng) }; 
		var rungis2 = { lat: 51.8581423, lng: 4.55385160000003 };
		


		var googlemaps = new google.maps.Map(mapContainer, {
			center: rungis,
			zoom: 15,
			disableDefaultUI: true,		
		  	styles: window.googleMapsStyling
		});

		var icon = {
			url: markerFolder + 'icon-marker_'+window.season+'.png',
			scaledSize: new google.maps.Size(34,45),
			origin: new google.maps.Point(0,0),
			anchor: new google.maps.Point(0,0)
		}
		
		marker = new google.maps.Marker({
			position: rungis,
			icon: icon
		});
		window.marker = marker;
		window.gm = googlemaps;
		marker.setMap(googlemaps);


	}else{


		var  geocoder = new google.maps.Geocoder();
		
		// Call the codeAddress function (once) when the map is idle (ready)
	    
		    // Define address to center map to
		    

	    geocoder.geocode({
	        'address': location
	    }, function (results, status) {

	        if (status == google.maps.GeocoderStatus.OK) {

	        	var googlemaps = new google.maps.Map(mapContainer, {
					center: results[0].geometry.location,
					zoom: 15,
					disableDefaultUI: true,		
				  	styles: window.googleMapsStyling
				});

				var icon = {
					url: markerFolder + 'icon-marker_'+window.season+'.png',
					scaledSize: new google.maps.Size(34,45),
					origin: new google.maps.Point(0,0),
					anchor: new google.maps.Point(0,0)
				}
				
				marker = new google.maps.Marker({
					position: results[0].geometry.location,
					icon: icon
				});
				marker.setMap(googlemaps);

	        	window.results = results;
	            // Center map on location
	            
	        } else {

	            console.log("Geocode was not successful for the following reason: " + status);
	        }
	    });
	   
	

	}
	

	
	
	
	google.maps.event.addDomListener(window, 'resize', function() {
		var center = googlemaps.getCenter();
		google.maps.event.trigger(googlemaps, 'resize');
		googlemaps.setCenter(center);
	});
	 
}

$(function() {
	
	$('.js-slider').each(function() {
		var _ = $(this);
		var slides = _.find('.slide');
		
		if(slides.length > 1) {
		
			_.owlCarousel({
				items: 1,
				nav: true,
				dots: true,
				//autoHeight: true,
				loop: true
			});
		}
	});
	
});
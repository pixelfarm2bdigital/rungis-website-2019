$(document).ready(function(){
	$(".cookiebar").each(function(){
		var _ = $(this);
		var $button = _.find(".btn");

		$button.on("click",function(e){
			e.preventDefault();
			Cookies.set('cookiesaccepted',(new Date()).toString(),{ expires: 365 });
			$(".cookiebar").slideUp();
		});
		if(!Cookies.get('cookiesaccepted')){
			$(".cookiebar").removeClass("is-hidden");
		}
	});
});
$(function() {
	
	$('.hero--large').each(function() {
		var _ = $(this);
		var slider = _.find('.hero__slider');
		var dotsContainer = _.find('.dots');
		
		if(slider.children().length > 1){
			slider.owlCarousel({
				items: 1,
				nav: false,
				dots: true,
				loop: true,
				margin:1,
				//animateOut: 'fadeOut',
				//responsive: false,
				mouseDrag: false,
				dotsContainer: dotsContainer,
				




				autoplay:true
			});
			
		}
			
	});
	
});
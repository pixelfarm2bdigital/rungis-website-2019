$(function() {
	
	$('.pd__slider').each(function() {
		var sync1 = $("#slider");
		var sync2 = $("#thumbnails");
		var syncedSecondary = true;
		if(!sync1 || sync1.children().length == 1){
			return;
		}
		sync1.owlCarousel({
		    items: 1,
		    loop: true,
		    nav: true,
		    dots: false,
		    loop: true
		}).on('changed.owl.carousel', syncPosition);

		sync2
		.on('initialized.owl.carousel', function () {
		    sync2.find(".owl-item .thumbs__item").eq(0).addClass("active");
		})
		.owlCarousel({
		    responsive: {
		    	0: {
		    		items: 3
		    	},
		    	400: {
		    		items: 3
		    	},
		    	640: {
		    		items: 5
		    	},
		    	768: {
		    		items: 5
		    	}
		    },
		    margin: 10,
		    nav: false,
		    dots: false,
		    loop: true
		}).on('changed.owl.carousel', syncPosition2);

		function syncPosition(el) {
		    //if you set loop to false, you have to restore this next line
		    //var current = el.item.index;

		    //if you disable loop you have to comment this block
		    var count = el.item.count-1;
		    var current = Math.round(el.item.index - (el.item.count/2) - .5);
		    if(current < 0) {
		        current = count;
		    }
		    if(current > count) {
		        current = 0;
		    }
		    //end block

		    sync2
		    .find(".thumbs__item")
		    .removeClass("active")
		    .eq(current)
		    .addClass("active");
		}

		function syncPosition2(el) {
		    if(syncedSecondary) {
		        var number = el.item.index;
		        sync1.data('owl.carousel').to(number, 100, true);
		    }
		}

		sync2.on("click", ".thumbs__item", function(e){
		    e.preventDefault();
		    sync1.trigger('to.owl.carousel', [$(e.target).parents('.owl-item').index(), 300, true]);
		});
	});
	
});
var windowWidth = $(window).width();
	
function createSlider() {
	$('.seasons').each(function() {
		var _ = $(this);
		if(windowWidth <= 500) {
			_.owlCarousel({
				items: 1,
				loop: true,
				nav: true,
				dots: true
			});
		}
	});
}


$(function() {
	
	createSlider();
	
	$(window).resize(function() {
		createSlider();
	});
	
});
angular.module( 'rungisApp', ['ui.router'] );
angular.module( 'rungisApp' ).run( [
	'$rootScope',
	'$state',
	'$stateParams',
	function ( $rootScope, $state, $stateParams ) {
		// References to $state and $stateParams to the $rootScope
		$rootScope.$state = $state;
		$rootScope.$stateParams = $stateParams;
	}
]);

angular.module( 'rungisApp' ).controller('ProductsController', ['$state','$stateParams', '$rootScope','$scope','$location','$http' ,'$element', function($state,$stateParams, $rootScope,$scope,$location,$http,$element) {

	var vm = this;

	vm.seasons = [];
	vm.seasonsTemp = [];


	vm.seasonsTemp.push({"slug":"winter","name":"Winter","internalName":"winter"});
	vm.seasonsTemp.push({"slug":"lente","name":"Lente","internalName":"spring"});
	vm.seasonsTemp.push({"slug":"voorzomer","name":"Voorzomer","internalName":"presummer"});
	vm.seasonsTemp.push({"slug":"zomer","name":"Zomer","internalName":"summer"});
	vm.seasonsTemp.push({"slug":"herfst","name":"Herfst","internalName":"autumn"});
	

	for(var i = 0;i<vm.seasonsTemp.length;i++){
		var season = vm.seasonsTemp[i];
		if(season.internalName == window.season){
			for(var j = i;j<vm.seasonsTemp.length+i;j++){
				var s = vm.seasonsTemp[j % vm.seasonsTemp.length];
				vm.seasons.push(s);
			}
			break;
		}
	}


	
	vm.scrollToTop = function(snap){
		$("html,body").animate({scrollTop:$(".producten__content").offset().top - $(".header__inner").height() - 60});
	};
	vm.mobileFilter = false;
	vm.toggleMobile = function(){
		$("html").toggleClass("is-fixed");
		vm.mobileFilter = !vm.mobileFilter;

		if(!vm.mobileFilter){
			$("html,body").scrollTop($(".producten__content").offset().top - $(".header__inner").height() - 60);
		}
		$(window).resize();
	};

	$(window).resize(function(){
		var $view = $(".list--products__mobile");
		var $button = $(".btn-container");

		$view.height($(window).height() - $view.position().top);
		$view.find(".scrollpane").css({"padding-bottom":$button.outerHeight()});
	}).resize();

	vm.dirty = false;

	vm.pageSize = 48; //was 24
	vm.pageNr = 1;
	vm.totalPages = 0;

	vm.products = [];


	$scope.$watch('vm.products',function(newVal){
		vm.totalPages = Math.ceil(vm.products.length / vm.pageSize);
	});
	$scope.$watch('vm.pageSize',function(newVal){
		vm.totalPages = Math.ceil(vm.products.length / vm.pageSize);
	});

	vm.pending = true;

	vm.categories = window.productCategories;

	

	vm.selectedCats = [];
	vm.selectedSeasons = [];
	vm.selectedBio = "alle";

	if($stateParams.filterCats != "alle"){
		//vm.selectedCats.push($stateParams.filterCats);
	}


	vm.toggleCat = function(cat){
		var index = vm.selectedCats.indexOf(cat);
		if(index === -1){
			vm.selectedCats.push(cat);		
		}else{
			vm.selectedCats.splice(index, 1);
		}
		if(vm.selectedCats.length > 1 && vm.selectedCats[0] == "alle"){
			vm.selectedCats.splice(0, 1);
		}
	};

	vm.toggleSeason = function(season){
		var index = vm.selectedSeasons.indexOf(season);
		if(index === -1){
			vm.selectedSeasons.push(season);		
		}else{
			vm.selectedSeasons.splice(index, 1);
		}
		if(vm.selectedSeasons.length > 1 && vm.selectedSeasons[0] == "alle"){
			vm.selectedSeasons.splice(0, 1);
		}
	};

	vm.forceValue = function(cat,value){


		var index = vm.selectedCats.indexOf(cat);
		
		
		if(index === -1){
			if(value){
				vm.selectedCats.push(cat);			
			}
		}else{
			if(!value){
				vm.selectedCats.splice(index, 1);	
			}
		}	
		
		
		if(vm.selectedCats.length > 1 && vm.selectedCats[0] == "alle"){
			vm.selectedCats.splice(0, 1);
		}

		
	}

	vm.toggleBio = function(){
		if(vm.selectedBio == "alle"){
			vm.selectedBio = "alleen-bio";
		}else{
			vm.selectedBio = "alle";
		}
	};

	vm.toggleMainCat = function(category){
		category.is_open = !category.is_open;

		/*
		if(category.sub){
			
			for(var i = 0;i<category.sub.length;i++){
		
				vm.toggleCat(category.sub[i].slug);

			}
			vm.updateFilter();
		}*/
	};
	vm.checkAllForMainCat = function(category,value){
		
		if(category.sub){
			
			for(var i = 0;i<category.sub.length;i++){
		
				vm.forceValue(category.sub[i].slug,value);
			}
		}

		if(vm.selectedCats.length > 1 && vm.selectedCats[0] == "alle"){
			vm.selectedCats.splice(0, 1);
		}
		vm.updateFilter();
	};

	vm.getSubCatsStatus = function(category){
		var active = true;
		for(var i = 0;i<category.sub.length;i++){
			var index = vm.selectedCats.indexOf(category.sub[i].slug);
			if(index === -1){
				//not found
				return false;
			}
		}
		return true;

	};

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){ 

		vm.selectedCats = toParams.filterCats.split(",");
		vm.selectedSeasons = toParams.filterSeasons.split(",");
		vm.selectedBio = toParams.filterBio;
		

		vm.pending = true;
		$http.post(ajax_object.ajax_url + '?action=getproducts', {
			cats : vm.selectedCats,
			seasons : vm.selectedSeasons,
			bio : vm.selectedBio
		}).
		success(function(data, status, headers, config) {
			
			vm.products = data.data;
			vm.pending = false;
			vm.pageNr = 1;
			vm.dirty = true;
			window.products = vm.products;
			// this callback will be called asynchronously
			// when the response is available
		}).
		error(function(data, status, headers, config) {
			vm.pending = false;
			vm.pageNr = 1;
			vm.dirty = true;
			// called asynchronously if an error occurs
			// or server returns response with an error status.
		});

	});
	
	vm.updateFilter = function(){
		var cats = vm.selectedCats;
		if(cats.length === 0){
			cats = 'alle'
		}

		var seasons = vm.selectedSeasons;
		if(seasons.length === 0){
			seasons = 'alle'
		}


		var bio = vm.selectedBio;
		if(bio.length === 0){
			bio = 'alle'
		}

		$state.go('producten', {
            filterCats: cats,
            filterSeasons: seasons,
            filterBio: bio
        });

       
	};

}]);	






angular.module( 'rungisApp' ).config( ['$stateProvider', '$urlRouterProvider', function( $stateProvider, $urlRouterProvider ) {
	
	$urlRouterProvider.otherwise( '/categorie/alle/seizoen/alle/bio/alle' );

	// States
	$stateProvider.
		state( 'producten', {
			url: '/categorie/:filterCats/seizoen/:filterSeasons/bio/:filterBio',
			controller: 'ProductsController'
		}); 
}]);

angular.module( 'rungisApp' ).filter('percentage', function() {
  return function(input) {
	return parseFloat(input / 100).toFixed(2).replace(".",",")+ "%";    
  };
});

angular.module( 'rungisApp').directive('a', function() {
	return {
		restrict: 'E',
		link: function(scope, elem, attrs) {
			if(attrs.ngClick || attrs.href === '' || attrs.href === '#'){
				elem.on('click', function(e){
					e.preventDefault();
				});
			}
		}
   };
}).filter('range', function() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
  };
}).filter('orderByPriority', function () {
  // custom value function for sorting
  function myValueFunction(item) {
  	if(item.prio == ""){
  		return -99999;
  	}else{
  		return item.prio;
  	}
  }

  return function (obj) {
    var array = [];
    Object.keys(obj).forEach(function (key) {
      // inject key into each object so we can refer to it from the template
      obj[key].name = key;
      array.push(obj[key]);
    });
    // apply a custom sorting function
    array.sort(function (a, b) {
      return myValueFunction(b) - myValueFunction(a);
    });
    return array;
  };
});
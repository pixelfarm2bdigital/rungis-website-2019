$(function() {
	
	$('.menu-mobile').each(function() {
		var _ = $(this);
		//var children = _.find('.menu-mobile__item');
		var hamburger = _.find('.hamburger');
		var cross = _.find('.cross');
		
		hamburger.on('touch click', function() {
			$('.menu-mobile').addClass('active');
			$(this).addClass('is-hidden');
			cross.removeClass('is-hidden');
			$('nav').addClass('open');
			$("html").addClass("is-fixed");
		});
		
		cross.on('touch click', function() {
			$('.menu-mobile').removeClass('active');
			$(this).addClass('is-hidden').removeClass('active');
			hamburger.removeClass('is-hidden');
			$('nav').removeClass('open');
			$("html").removeClass("is-fixed");
		});		
	});
	
});
var windowWidth = $(window).width();

function createMediaSlider() {
	// homepage
	$('.media.slider').each(function() {
		
		var _ = $(this);
		
		if(windowWidth <= 768) {
			
			_.owlCarousel({
				items: 1,
				loop: true,
				nav: true,
				dots: true,

			});
		}		
	});
}
	
function createInspiratieSlider() {
	// inspiratie pagina
	$('.media__items.slider').each(function() {
		var _ = $(this);
		
		if(windowWidth <= 666) {
			_.owlCarousel({
				items: 1,
				loop: true,
				nav: true,
				dots: false
			});
		}
	});
}



$(function() {
	
	createMediaSlider();
	createInspiratieSlider();
	
	$(window).resize(function() {
		createMediaSlider();
		createInspiratieSlider();
	});
	
});
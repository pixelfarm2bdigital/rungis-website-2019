$(function() {
	
	$('.order').each(function() {
		var _ = $(this);
		var trigger = _.find('.trigger');
		var options = _.find('.options');
		var bestelbox = _.find('.bestelbox');
		var toggleBestelbox = _.find('.subtrigger.business');
		
		// hide options by default
		options.hide();
		bestelbox.hide();
		
		// show options on trigger-click
		trigger.on('touch click', function(e) {
			options.slideToggle('fast');
			e.stopPropagation();
		});
		
		toggleBestelbox.on('touch click', function(e) {
			$(this).toggleClass('open');
			bestelbox.slideToggle('fast');
			e.stopPropagation();
		});
		
		// close orderbox on outside click
		$(document).on('touch click', function() {
			options.hide();
			bestelbox.hide();
			toggleBestelbox.removeClass('open');
		});
	});
	
	$('.order--large').each(function() {
		var _ = $(this);
		var options = _.find('.options');
		
		options.show();
		
		$(document).on('touch click', function() {
			options.show();
			
		});	
	});
	
});
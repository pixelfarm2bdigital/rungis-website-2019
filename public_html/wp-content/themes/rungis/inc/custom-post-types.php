<?php


/*
 * Custom posttypes
 */
function create_posttype() {
	register_post_type('persoon',
        array(
        	'labels' => array(
            	'name' => ('Personen'),
                'singular_name' => ('Persoon')
            ),
            'public' => true,
            'has_archive' => true,
            'menu_icon'=>'dashicons-groups',


            'rewrite' => array(
                'slug' => 'personen'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'),
        )
	);
	
	register_post_type('product',
        array(
        	'labels' => array(
            	'name' => ('Producten'),
                'singular_name' => ('Product')
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'=>'dashicons-carrot',

            'taxonomies' => array(
                 'productcategory'
            ),
            'rewrite' => array(
                'slug' => 'product'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'),
        )
	);
	
	register_post_type('persbericht',
        array(
        	'labels' => array(
            	'name' => ('Persberichten'),
                'singular_name' => ('Persbericht')
            ),
            'public' => true,
            'has_archive' => false,
            'menu_icon'=>'dashicons-megaphone',
            'taxonomies' => array(
                 'publicatietype'
            ),
            'rewrite' => array(
                'slug' => 'persbericht'),
            'supports' => array(
                'title',
                'editor',
                'thumbnail'),
        )
	);
	

	register_post_type('quote',
        array(
        	'labels' => array(
            	'name' => ('Quotes'),
                'singular_name' => ('Quote')
            ),
            'public' => true,
            'menu_icon'=>'dashicons-thumbs-up',
            'has_archive' => false,
            'taxonomies' => array(
                 
            ),
            'rewrite' => false,
            'supports' => array(
                'title',
                'thumbnail'
            ),
        )
	);
	


	




}
add_action('init', 'create_posttype');
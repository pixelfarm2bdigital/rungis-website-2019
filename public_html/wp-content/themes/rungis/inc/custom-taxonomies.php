<?php



// Register Custom Taxonomy
function register_taxonomies() {

	$labels = array(
		'name'                       => ( 'Productcategories' ),
		'singular_name'              => ( 'Productcategory' ),
		'menu_name'                  => ( 'Productcategory' ),
		'all_items'                  => ( 'All productcategories' ),
		'parent_item'                => ( 'Parent Category' ),
		'parent_item_colon'          => ( 'Parent Category:' ),
		'new_item_name'              => ( 'New Productcategory' ),
		'add_new_item'               => ( 'Add New Productcategory' ),
		'edit_item'                  => ( 'Edit Productcategory' ),
		'update_item'                => ( 'Update Productcategory' ),
		'view_item'                  => ( 'View Productcategory' ),
		'separate_items_with_commas' => ( 'Separate items with commas' ),
		'add_or_remove_items'        => ( 'Add or remove items' ),
		'choose_from_most_used'      => ( 'Choose from the most used' ),
		'popular_items'              => ( 'Popular Items' ),
		'search_items'               => ( 'Search Items' ),
		'not_found'                  => ( 'Not Found' ),
		'no_terms'                   => ( 'No items' ),
		'items_list'                 => ( 'Items list' ),
		'items_list_navigation'      => ( 'Items list navigation' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'productcategory', array( 'product' ), $args );


	$labels = array(
		'name'                       => ( 'Publicatietypes' ),
		'singular_name'              => ( 'Publicatietype' ),
		'menu_name'                  => ( 'Publicatietypes' ),
		'all_items'                  => ( 'Alle publicatietypes' ),
		'parent_item'                => ( 'Parent Category' ),
		'parent_item_colon'          => ( 'Parent Category:' ),
		'new_item_name'              => ( 'New type' ),
		'add_new_item'               => ( 'Add New Type' ),
		'edit_item'                  => ( 'Edit type' ),
		'update_item'                => ( 'Update type' ),
		'view_item'                  => ( 'View Type' ),
		'separate_items_with_commas' => ( 'Separate items with commas' ),
		'add_or_remove_items'        => ( 'Add or remove items' ),
		'choose_from_most_used'      => ( 'Choose from the most used' ),
		'popular_items'              => ( 'Popular Items' ),
		'search_items'               => ( 'Search Items' ),
		'not_found'                  => ( 'Not Found' ),
		'no_terms'                   => ( 'No items' ),
		'items_list'                 => ( 'Items list' ),
		'items_list_navigation'      => ( 'Items list navigation' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
	);
	register_taxonomy( 'publicatietype', array( 'persbericht' ), $args );


	


}
add_action( 'init', 'register_taxonomies', 0 );

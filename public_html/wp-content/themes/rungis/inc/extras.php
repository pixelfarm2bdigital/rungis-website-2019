<?php
/**
 * Custom functions that act independently of the theme templates.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Rungis
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function rungis_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'rungis_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function rungis_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', bloginfo( 'pingback_url' ), '">';
	}
}
add_action( 'wp_head', 'rungis_pingback_header' );



function getimagesforproduct($wpid = null){
	
	return getimagesforproductbyprodnumber(get_field("reference",$wpid));
}


function getimagesforproductbyprodnumber($productid){
	$dir = WP_CONTENT_DIR."/uploads/products/";
	$url = WP_CONTENT_URL."/uploads/products/";
	$glob = $dir.$productid."_*";
	
	$files = glob($glob);
	foreach($files as  $key =>$file){
		$files[$key] = $url.basename($file);
	}
	return $files;
}


add_filter( 'gform_cdata_open', 'wrap_gform_cdata_open' );
function wrap_gform_cdata_open( $content = '' ) {
	$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
	return $content;
}
add_filter( 'gform_cdata_close', 'wrap_gform_cdata_close' );
function wrap_gform_cdata_close( $content = '' ) {
	$content = ' }, false );';
	return $content;
}
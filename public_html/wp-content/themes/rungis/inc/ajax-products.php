<?php




add_action( 'wp_ajax_nopriv_getproducts', 'getproducts_query' );
add_action( 'wp_ajax_getproducts', 'getproducts_query' );

function getproducts_query() {
	global $wpdb;
	
	$request_body = file_get_contents('php://input');

	$requests = json_decode($request_body);


	$wpdb->show_errors();
	$products = array();
	$args = array(
		'post_type' 		=> 'product',
		'posts_per_page' 	=> -1,
		"post_status"			=> "publish",
		
		'orderby'  			=> array( 
								'meta_value_num' => 'ASC', 
								'title' => 'ASC'
		),
		'meta_key' 			=> 'top_sellers',
		


	);
	
	$cats = $requests->cats;
	if($cats[0] == "alle"){
		$cats = false;
	}

	$seasons = $requests->seasons;
	if($seasons[0] == "alle"){
		$seasons = false;
	}else{
		$seasons = $seasons;
	}

	$alleenBio = false;
	if($bio = $requests->bio){
		if($bio == "alleen-bio"){
			$alleenBio = true;
		}
	}
	//print_r($seasons);

	if($alleenBio || $seasons){

		$args['meta_query']	= array(
			'relation' =>"AND"
		);

		if($alleenBio){
			$args['meta_query'][] = array(
				'key'	  	=> 'biologisch',
				'value'	  	=> 1,
				'compare' 	=> '=',
			);
		}

		if($seasons){

			$seasonsArgs = array(
				"relation" => "OR"
			);
			

			foreach($seasons as $season){
				switch ($season) {
					case 'lente':
						$season = "spring";
						break;
					case 'voorzomer':
						$season = "presummer";
						break;
					case 'zomer':
						$season = "summer";
						break;
					case 'herfst':
						$season = "autumn";
						break;

					default:
						# code...
						break;
				}

				$seasonsArgs[] = array(
					'key'	  	=> $season,
					'value'	  	=> 1,
					'compare' 	=> '=',
				);

			}

			$args['meta_query'][] = $seasonsArgs;

		}


	}

	if($cats){

		$args['tax_query'] = array();

		if($cats && $seasons){
			$args['tax_query']['relation'] = "AND";
		}

		if ($cats) {
			$args['tax_query'][] = array(
				"taxonomy"=>"productcategory",
				"field"=>"slug",
				"terms"=>$cats
			);
		}
	}


	//print_r($args);
	
	$imagepaths = wp_get_upload_dir();
	$basedir = $imagepaths['basedir'];
	$baseurl = $imagepaths['baseurl'];
	
	
	
	
	$producten = new WP_Query($args);
	while($producten->have_posts()) : $producten->the_post();
		//$image = get_field('persoon_image');
		




		if(IMGFIX){


			$id = get_the_ID();

			$imageSrc = getimagesforproduct($id);
			$imageSrc = $imageSrc[0];
		}else{
			$attachmentId = get_post_thumbnail_id();
			$imageSrc = wp_get_attachment_image_src( $attachmentId, 'medium' );
			if($imageSrc[0]){
				$imageSrc = $imageSrc[0];
			}
			
		}
		

	

		
		$categories = get_the_terms(null, "productcategory" );


		$product = array();
		$product['permalink'] = html_entity_decode(get_permalink());
		$product['title'] =  html_entity_decode(get_the_title());
		$product['category'] = $categories[0]->name;
		$product['image'] = $imageSrc;
		$product['prio'] = get_field("top_sellers");
		

		


		$products[] = $product;

	endwhile;
	wp_reset_query();

	$returndata = array();

	
	
	$returndata['data'] = $products;
	header('Content-Type: application/json');
	echo json_encode($returndata);
	
	
	wp_die();
}
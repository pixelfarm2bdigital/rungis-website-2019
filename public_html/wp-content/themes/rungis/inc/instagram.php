<?php

require __DIR__.'/../../../../../vendor/autoload.php';

function parsePostsBlock($html){
    $posts = array();
    $html = substr($html, strpos($html, "{"));
    $html = trim($html,";");
    $rawObject = json_decode($html,true);


    $userInfo = recursive_array_search("user", $rawObject);
    $rawObject = recursive_array_search("media", $rawObject);

    $pageInfo = $rawObject['page_info'];
    $nodes = $rawObject['nodes'];
    $data = array();
    $data['date'] = $nodes[count($nodes) - 1]['date'];
    $data['nextid'] = $pageInfo['end_cursor'];
    $data['posts'] = $nodes;
    $data['userinfo'] = $userInfo;

    return $data;



}

function getInstagramPosts($username,$id = false){
    try{
        \InstagramAPI\Instagram::$allowDangerousWebUsageAtMyOwnRisk = true;

        $user = 'pixelfarm_nl';
        $password = 'PXLFRM0180';

        $ig = new InstagramAPI\Instagram(0, 0);

        try{
            $ig->login($user, $password);
        }
        catch (Exception $e)
        {
//        echo '<h1>nope</h1>';
        }
        $userId = $ig->people->getUserIdForName($username);
        $userFeed = $ig->timeline->getUserFeed($userId,$id);
        $feed = $userFeed->getItems();

        $instaArray = [];
        foreach ($feed as $item)
        {
//            return
            $versions =$item->getImageVersions2();
            if($versions) {
                $instaArray['posts'][] = ['display_src' => $versions->getCandidates()[0]->getUrl()];
            }
        }
        return $instaArray;

    }catch(Exception $e){

    }
    return false;

}

function getInstPost(){

    $insta = get_transient( 'instagram_posts' );

    if ( false === ( $result = $insta ))  {
        // It wasn't there, so regenerate the data and save the transient
        $result = getInstagramPosts("rungis_bv");
//	    set_transient( 'instagram_posts', $result, 60*10 );
    }

    return $result['posts'][0]['display_src'];
}

function recursive_array_search($needle,$haystack) {
    foreach($haystack as $key=>$value) {
        $current_key=$key;
        if($needle===$key){
            return $value;
        }


        if(is_array($value)){
            $recsearch = recursive_array_search($needle,$value);
            if($recsearch !== false){
                return $recsearch;
            }
        }
    }
    return false;
}


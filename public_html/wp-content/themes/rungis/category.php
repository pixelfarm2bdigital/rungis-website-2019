<?php

get_header() ?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo single_term_title(); ?></h1>
		</div>
	</div>
	
	<div class="media">
		<div class="persberichten">
			<?php

			while ( have_posts() ) : the_post();
			
				//$image = get_field('persoon_image');
				
				$attachmentId = get_post_thumbnail_id();
				$category = get_the_terms( null, "publicatietype");


				

				$imageSrc = wp_get_attachment_image_src( $attachmentId, 'medium' );
				if($imageSrc[0]){
					$imageSrc = $imageSrc[0];
				}

				//override thumbnail
				$thumbnailOverride = get_field("thumbnail_image");
				if($thumbnailOverride){
					$imageSrc = $thumbnailOverride['sizes']['medium'];
				}

				if(get_field("persberichten_link")['url']){
					$link = get_field("persberichten_link")['url'];
					
				}elseif(get_field("persberichten_file")){
					$link = get_field("persberichten_file")['url'];
					
				}else{
					$link = get_permalink();
				}

			?>
			
				<a href="<?php echo $link;?>" class="persberichten__item">
					<div class="persberichten__item__inner">						
						<div class="persberichten__item__media">
							<div style="background-image: url(<?php echo $imageSrc;?>);"></div>
						</div>
						<div class="persberichten__item__content">
							<p class="persberichten__item__content__header">
								<span class="date"><?php echo get_the_date('F Y'); ?></span>
								<span class="category"><?php echo @$category[0]->name;?></span>
							</p>
							<?php echo the_title(); ?>
							
							<!-- Generator: Adobe Illustrator 21.0.2, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
							<svg version="1.1" id="Isolation_Mode" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
								 y="0px" viewBox="0 0 15 27" style="enable-background:new 0 0 15 27;" xml:space="preserve">
							<style type="text/css">
								.st0{fill:#57ac25;}
							</style>
							<path class="st0" d="M0.4,24.4c-0.6,0.6-0.6,1.5,0,2.1c0.6,0.6,1.5,0.6,2.1,0l12-12c0.6-0.6,0.6-1.6,0-2.1l-12-12
								C2-0.1,1-0.1,0.4,0.4C-0.1,1-0.1,2,0.4,2.6l10.9,10.9L0.4,24.4L0.4,24.4z"/>
							</svg>
						</div>
					</div>
				</a>
			
			<?php
				endwhile;
				
			?>
			<?php hm_get_template_part('parts/pagination'); ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rungis
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TZZKTVW');</script>
<!-- End Google Tag Manager -->
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-57x57.png?v=1">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-60x60.png?v=1">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-72x72.png?v=1">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-76x76.png?v=1">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-114x114.png?v=1">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-120x120.png?v=1">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-144x144.png?v=1">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-152x152.png?v=1">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/apple-icon-180x180.png?v=1">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri();?>/assets/img/favicon/android-icon-192x192.png?v=1">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/favicon-32x32.png?v=1">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/favicon-96x96.png?v=1">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/favicon-16x16.png?v=1">
<link rel="manifest" href="<?php echo get_template_directory_uri();?>/assets/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri();?>/assets/img/favicon/ms-icon-144x144.png?v=1">
<meta name="theme-color" content="#ffffff">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57442738-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<?php

	$currentSeason = CURRENTSEASON;
?>

<body <?php body_class( array(CURRENTSEASON) ); ?>>
<script>
var season = "<?php echo CURRENTSEASON ;?>";

if(season === ''){
	season = 'winter';
}

if(window.season === "winter"){
	var colorPrimary = "#e4c6d5";
}
if(window.season === "spring"){
	var colorPrimary = "#cd0a39";
}
if(window.season === "presummer"){
	var colorPrimary = "#B1D698";
}
if(window.season === "summer"){
	var colorPrimary = "#E6D4BE";
}
if(window.season === "autumn"){
	var colorPrimary = "#C99891";
}

</script>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZZKTVW"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="page" class="site">
		
		<?php
		/*
		<header id="masthead" class="site-header" role="banner">
			<div class="site-branding">
				<?php
				if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
				endif;

				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; // WPCS: xss ok. // ?></p>
				<?php
				endif; ?>
			</div><!-- .site-branding -->

			<nav id="site-navigation" class="main-navigation" role="navigation">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'rungis' ); ?></button>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->
		*/
		?>
		
		<header class="header">
			<div class="header__inner">
				<a href="<?php echo esc_url(home_url('/')); ?>" class="logo">
					<img alt="Rungis" src='<?php echo get_template_directory_uri(); ?>/assets/img/logo-rungis-2.svg' />	
				</a>
				
				<nav>
					<span class="screen-reader-text">Hoofd navigatie</span>
					<div class='scroll-container'>
						<div class='menu-wrapper'>
							<div class='orderbuttons is-mobile'>
								<?php
								hm_get_template_part("parts/orderbuttons");
								?>
							</div>
							<?php wp_nav_menu(array('theme_location' => 'main', 'menu_id' => 'primary_menu')); ?>
							<form class="is-mobile form form--search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
								<input id="search" type="text" name="s" value="<?php echo @$_GET['s'];?>" placeholder="<?php echo __("Zoeken","rungis");?>"/
								><input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/
								><input type="submit" value=""/>
							</form>
						</div>
					</div>
					
				</nav>
				<div class="topcontent">
					<div class="topcontent__inner">
						<!--<a href="/" class="btn">Bestellen</a>-->
						
						
						<?php
						hm_get_template_part("parts/orderbuttons");
						?>
						<?php
						hm_get_template_part("parts/languageswitch");
						?>
						
						<form class="form form--search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
							<input id="search" type="text" name="s" value="<?php echo @$_GET['s'];?>" placeholder="<?php echo __("Zoeken","rungis");?>"/>
							<input type="hidden" name="lang" value="<?php echo(ICL_LANGUAGE_CODE); ?>"/>
							<input type="submit" value=""/>
						</form>
					</div>
				</div>
				<div class='is-mobile languageswitch' style=''>
				<?php
					$languages = icl_get_languages();
					foreach($languages as $lang){
						if($lang['active']) continue;
						?>
						<a href="<?php echo $lang['url'];?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flag-<?php echo $lang['language_code'];?>.png"/></a>
						<?php
					}
					?>
				</div>
				<div class="menu-mobile">

					<div class="menu-mobile__item hamburger"><svg xmlns="http://www.w3.org/2000/svg" height="32" viewBox="0 0 32 32" width="32"><path d="M4 10h24c1.1 0 2-0.9 2-2s-0.9-2-2-2H4C2.9 6 2 6.9 2 8S2.9 10 4 10zM28 14H4c-1.1 0-2 0.9-2 2s0.9 2 2 2h24c1.1 0 2-0.9 2-2S29.1 14 28 14zM28 22H4c-1.1 0-2 0.9-2 2s0.9 2 2 2h24c1.1 0 2-0.9 2-2S29.1 22 28 22z"/></svg></div>
					<div class="menu-mobile__item cross is-hidden"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M22.2 4c0.3 0.3 0.3 0.8 0 1.1l-6.3 6.3c-0.3 0.3-0.3 0.8 0 1.1l6.3 6.3c0.3 0.3 0.3 0.8 0 1.1l-2.3 2.3c-0.3 0.3-0.8 0.3-1.1 0l-6.3-6.3c-0.3-0.3-0.8-0.3-1.1 0l-6.3 6.3c-0.3 0.3-0.8 0.3-1.1 0l-2.3-2.3c-0.3-0.3-0.3-0.8 0-1.1l6.3-6.3c0.3-0.3 0.3-0.8 0-1.1L1.7 5.1c-0.3-0.3-0.3-0.8 0-1.1l2.3-2.3C4.3 1.4 4.8 1.4 5.1 1.7L11.4 8c0.3 0.3 0.8 0.3 1.1 0l6.3-6.3c0.3-0.3 0.8-0.3 1.1 0L22.2 4z"/></svg></div>
				</div>
			</div>
		</header>

		<div id="content" class="site-content">

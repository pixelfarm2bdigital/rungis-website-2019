<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Rungis
 */

?>

	</div> <!-- /site content -->
</div> <!-- /site -->

<?php 
if(is_404()) {
	// is 404 page
} else {
	$quotes = get_posts(array(
		"post_type"			=>	"quote",
		"posts_per_page"	=>	2,
		"orderby" 			=> "rand",
		'suppress_filters' => false
	));

	if(!empty($quotes)){
		$quotesBackground = get_field("quotes_background","option");
	?>
	<div class="quotes">
		<div class="quotes__inner" style="background-image: url('<?php echo $quotesBackground['sizes']['hero_slider'];?>');">
			<div class="quotes__wrapper">
				<?php foreach($quotes as $quote){
					hm_get_template_part('parts/chef-quote',
						array(
							'name' 		=> get_field("name",$quote),
							'jobtitle'  => get_field("jobtitle",$quote),
							'quote'		=> get_field("quote",$quote),
							'image'		=> get_post_image_src('portrait',$quote->ID)
						)
					);
				}
				?>
			</div>
		</div>
	</div>
	<?php } ?>
	
	<footer class="footer" id="nieuwsbrief">
		<div class="row nogutter">
			<div class="col col-65 pod pod--sec"> 
				<div class="pod__inner">
					<div class="row">
						<div class="col col-60 pl-l">
							<a href="<?php echo esc_url(home_url('/')); ?>" class="logo">
								<img alt="Rungis" width="143" height="65" src='<?php echo get_template_directory_uri(); ?>/assets/img/logo-rungis-2-white.svg' />	
							</a>
							<p class="intro"><?php echo get_field("footer_small_about","option");?></p>

							<a href="<?php echo get_url_for_language('/over-rungis/');?>"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></a>

							<!-- ERWIN DEBUG SEIZOENEN -->
							<?php 
							/*
							?>

							<div class="debugbar">
								<a href="." id="winter" onClick="Cookies.set('season','winter',{ expires: 365 })">winter</a>
								<a href="." id="spring" onClick="Cookies.set('season','spring',{ expires: 365 })">lente</a>
								<a href="." id="presummer" onClick="Cookies.set('season','presummer',{ expires: 365 })">voorzomer</a>
								<a href="." id="summer" onClick="Cookies.set('season','summer',{ expires: 365 })">zomer</a>
								<a href="." id="autumn" onClick="Cookies.set('season','autumn',{ expires: 365 })">herfst</a>
							</div>
							<!-- END DEBUG SEIZOENEN -->
							<?php 

							*/
							?>

						</div>
						<div class="col col-40 pl">
							<div class="footer__menu">
								<p class="footer__menu__title is-desktop"><?php echo __("Keuzes","rungis");?></p>
								<?php wp_nav_menu(array('theme_location' => 'footer', 'menu_id' => 'FooterMenu')); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col col-35 pod pod--tert">
				<div class="pod__inner">
					<div class="form form--newsletter">
						<p class="form__title"><?php echo __("Aanmelden nieuwsbrief","rungis");?></p>
						<?php
						//gravity_form( $id_or_title, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );
						?>
						<?php echo gravity_form(1,false,false,false,'',true,99999); ?>
					</div>
				</div>
			</div>
		</div>

		<div class="pod pod--pri">
			<div class="row">
				<div class="col col-33">
					<a href="<?php echo get_url_for_language('/kwaliteitszorg/');?>"  class="footerlogo logo-iso"><img  alt="ISO" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-iso.png"/></a>
					<a href="<?php echo get_url_for_language('/kwaliteitszorg/');?>"  class="footerlogo logo-skal"><img  alt="Skal" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo-skal.png"/></a>				
				</div>
				<div class="col col-33 center">
					<a href="<?php echo get_url_for_language('/algemene-voorwaarden/');?>"><?php echo __("Algemene voorwaarden","rungis");?></a>
				</div>
				<div class="col col-33 center">
					<a href="https://www.instagram.com/rungis_bv/" class="social instagram" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 458.9 458.9"><path d="M404.8 0H55.7C25.7 0 0 28.3 0 58.2V407.5c0 30 25.7 51.4 55.7 51.4H404.8c30.1 0 54.1-21.4 54.1-51.4V58.2C458.9 28.3 434.8 0 404.8 0zM394 65.6c8.7 0 15.8 7.1 15.8 15.8v50.4c0 8.7-7.1 15.8-15.8 15.8h-50.4c-8.7 0-15.8-7.1-15.8-15.8V81.3c0-8.7 7.1-15.8 15.8-15.8H394zM230.8 145.6c48.6 0 88.1 39.3 88.1 87.8 0 48.5-39.4 87.8-88 87.8 -48.6 0-88-39.3-88-87.8C142.7 184.9 182.1 145.6 230.8 145.6zM409.7 395.8c0 7-7.5 13.9-14.6 13.9H66.4c-7.1 0-17.2-6.9-17.2-13.9V196.7h47.3c-3.2 16.4-5 24-5 36.7 0 76.6 62.5 138.9 139.3 138.9 76.8 0 139.3-62.3 139.3-138.9 0-12.7-1.7-20.3-4.9-36.7h44.6V395.8z"/></svg>
						<span class="screen-reader-text"><?php echo __("Volg Rungis op Instagram","rungis");?></span>
					</a>
					
					<a href="https://www.linkedin.com/company/rungis-b.v." class="social linkedin" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310 310"><path d="M72.2 99.7H9.9c-2.8 0-5 2.2-5 5v199.9c0 2.8 2.2 5 5 5H72.2c2.8 0 5-2.2 5-5V104.7C77.2 102 74.9 99.7 72.2 99.7z"/><path d="M41.1 0.3C18.4 0.3 0 18.7 0 41.4 0 64 18.4 82.4 41.1 82.4c22.6 0 41-18.4 41-41C82.1 18.7 63.7 0.3 41.1 0.3z"/><path d="M230.5 94.8c-25 0-43.5 10.7-54.7 23V104.7c0-2.8-2.2-5-5-5h-59.6c-2.8 0-5 2.2-5 5v199.9c0 2.8 2.2 5 5 5h62.1c2.8 0 5-2.2 5-5v-98.9c0-33.3 9.1-46.3 32.3-46.3 25.3 0 27.3 20.8 27.3 48v97.2c0 2.8 2.2 5 5 5H305c2.8 0 5-2.2 5-5V195C310 145.4 300.5 94.8 230.5 94.8z"/></svg>
						<span class="screen-reader-text"><?php echo __("Volg Rungis op LinkedIn","rungis");?></span>
					</a>
					
					<a href="https://twitter.com/rungis_bv" class="social twitter" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 612 612"><path d="M612 116.3c-22.5 10-46.7 16.8-72.1 19.8 25.9-15.5 45.8-40.2 55.2-69.4 -24.3 14.4-51.2 24.8-79.8 30.5 -22.9-24.4-55.5-39.7-91.6-39.7 -69.3 0-125.6 56.2-125.6 125.5 0 9.8 1.1 19.4 3.3 28.6C197.1 206.3 104.6 156.3 42.6 80.4c-10.8 18.5-17 40.1-17 63.1 0 43.6 22.2 82 55.8 104.5 -20.6-0.7-39.9-6.3-56.9-15.8v1.6c0 60.8 43.3 111.6 100.7 123.1 -10.5 2.8-21.6 4.4-33.1 4.4 -8.1 0-15.9-0.8-23.6-2.3 16 49.9 62.3 86.2 117.3 87.2 -42.9 33.7-97.1 53.7-155.9 53.7 -10.1 0-20.1-0.6-29.9-1.7 55.6 35.7 121.5 56.5 192.4 56.5 230.9 0 357.2-191.3 357.2-357.2l-0.4-16.3C573.9 163.5 595.2 141.4 612 116.3z" fill="#010002"/></svg>
						<span class="screen-reader-text"><?php echo __("Volg Rungis op Twitter","rungis");?></span>
					</a>
					
					<a href="https://www.facebook.com/Rungisbv/" class="social facebook" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 310 310"><path d="M81.7 165.1h34V305c0 2.8 2.2 5 5 5h57.6c2.8 0 5-2.2 5-5V165.8h39.1c2.5 0 4.7-1.9 5-4.4l5.9-51.5c0.2-1.4-0.3-2.8-1.2-3.9 -0.9-1.1-2.3-1.7-3.7-1.7h-45V72c0-9.7 5.2-14.7 15.6-14.7 1.5 0 29.4 0 29.4 0 2.8 0 5-2.2 5-5V5c0-2.8-2.2-5-5-5h-40.5C187.5 0 186.8 0 185.9 0c-7 0-31.5 1.4-50.8 19.2 -21.4 19.7-18.4 43.3-17.7 47.4v37.8H81.7c-2.8 0-5 2.2-5 5v50.8C76.7 162.9 78.9 165.1 81.7 165.1z"/></svg>
						<span class="screen-reader-text"><?php echo __("Volg Rungis op Facebook","rungis");?></span>
					</a>
				</div>
			</div>
		</div>
	</footer>
<?php } ?>

<div class='cookiebar is-hidden'>
	<div class='wrap'>	
		
		<div class='cms-content'>
			<?php echo get_field("cookiemessage","option"); ?>
		</div>
		<a href="#" class='btn btn--pri'><?php echo __('Ok',"rungis");?></a>
		
	</div>
</div>

<?php wp_footer(); ?>


<?php if(is_page('Contact') || is_singular('product')) : ?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCyGsWQz6EACCb8zpYifvaz5Gu8eKHTzKg&callback=initMap"></script>
<?php endif; ?>
<style>
footer.footer .form--newsletter .gform_wrapper .top_label input.medium{
	padding-left:40px !important;
}

.gform_wrapper ul.gfield_radio li label{
	margin-left:24px;
}
</style>
</body>
</html>

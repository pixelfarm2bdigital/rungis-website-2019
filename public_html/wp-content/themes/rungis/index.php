<?php
/*
 * Template Name: Inspiratie
 */



get_header(); ?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo get_field("title",get_queried_object());?></h1>
			<?php echo get_field("intro",get_queried_object());?>
		</div>
	</div>
	<?php
	function getCat($catSlug){

		if(ICL_LANGUAGE_CODE != "nl"){
			return ICL_LANGUAGE_CODE."/".$catSlug."-".ICL_LANGUAGE_CODE;
		}

		return $catSlug;
	}
	?>
	<div class="media">
		
		<div class="media__filter">
			
			<div class="media__filter__item">
				<a href="/<?php echo getCat("blog");?>/"><?php echo __("Blogs","rungis");?></a>
			</div>
			<div class="media__filter__item">
				<a href="/<?php echo getCat("smaak");?>/"><?php echo __("De smaak van de chef","rungis");?></a>
			</div>
			<div class="media__filter__item">
				<a href="/<?php echo getCat("video");?>/"><?php echo __("Gert Jan's keuze","rungis");?></a>
			</div>
			<div class="media__filter__item">
				<a href="/<?php echo getCat("column");?>/"><?php echo __("Column","rungis");?></a>
			</div>
		</div>

	
		
		<div class="media__items ">
			

			<?php

			$perPage = 12;


		if ( have_posts() ) : ?>
			<?php
			$inspiratiePosts = array();


			function getPostWithWidthAndType($width,$type = ""){
				global $inspiratiePosts;
				
				foreach($inspiratiePosts as $key => $p){
					if($p['width'] == $width && (!$type || $type == $p['type'] )){
						array_splice($inspiratiePosts, $key, 1);
						return $p;
					}
				}
			}

			function renderPostWithWidthAndType($width,$type = ""){
				$p = getPostWithWidthAndType($width,$type);

				if($p){
					$p['return'] = true;	
				}
				
				
				$t = "";
				if($p){
					$t .= hm_get_template_part('parts/'.$p['layout'],$p);
				}
				if(!$p && $width == 2){
					$p = getPostWithWidthAndType(1);
					if($p){
					$p['return'] = true;	

						$t .= hm_get_template_part('parts/'.$p['layout'],$p);
					}
					
					$p = getPostWithWidthAndType(1);
					if($p){
					$p['return'] = true;	

						$t .= hm_get_template_part('parts/'.$p['layout'],$p);	
					}
				}
				return $t;

				
			}

			/* Start the Loop */
			while ( have_posts() ) : the_post();


				$label = get_the_category()[0]->name;
				$width = 1;
				$image = get_post_image_src();

				//override thumbnail
				$thumbnailOverride = get_field("thumbnail_image");
				if($thumbnailOverride){
					$image = $thumbnailOverride['sizes']['large'];
				}
				if(has_category("smaak-van-de-chef" ) || has_category("smaak") || has_category("smaak-en")){
					$width = 2;
					$type = "svdc";
					$layout = "media_item_large";
				
					
				}elseif(has_category("gertjans-keuze" )|| has_category("film") || has_category("video") || has_category("video-en")){
					$width = 2;
					$type = "gk";
					$layout = "media_item_video_large";



				}else{
					$width = 1;
					$type = "normal";
					$layout = "media_item";
				}





				
				$itemDetails = array(
					'title'=>get_the_title(),
					'link' => get_permalink(),
					'image'=> $image,
					'label'=>$label,
					'width'=>$width,
					'layout'=>$layout,
					'type'=>$type
				);

				$inspiratiePosts[] = $itemDetails;


			endwhile;

			if(isset($_GET['pagina'])){
				$currentPage = intval($_GET['pagina']);	
			}else{
				$currentPage = 1;
			}
			

			//$inspiratiePosts = array_slice($inspiratiePosts, $currentPage * $perPage,$perPage);

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif;
			$theContents = array();


			while(count($inspiratiePosts) > 0){
				$theContents[] = renderPostWithWidthAndType(2,"svdc");
				$theContents[] = renderPostWithWidthAndType(1);
				$theContents[] = renderPostWithWidthAndType(1);
				$theContents[] = renderPostWithWidthAndType(2,"gk");
			}

			$totalItems = count($theContents);
			
			$totalPages = ceil($totalItems / $perPage);



			
			$theContents = array_slice($theContents, ($currentPage - 1) * $perPage,$perPage);



			foreach($theContents as $key=> $content){
				echo $content;
			}



			/*
			if(has_category("smaak-van-de-chef" )){
				hm_get_template_part('parts/media_item_large',$itemDetails);	
			}elseif(has_category("gertjans-keuze" )){
				hm_get_template_part('parts/media_item_video_large',$itemDetails);
			}else{
				hm_get_template_part('parts/media_item',$itemDetails);
			}



			//smaak van de chef
			
			
			
			hm_get_template_part('parts/media_item');
			hm_get_template_part('parts/media_item');
			hm_get_template_part('parts/media_item');
			
			hm_get_template_part('parts/media_item');
			hm_get_template_part('parts/media_item_video_large');
			
			hm_get_template_part('parts/media_item_large');
			hm_get_template_part('parts/media_item');
			
			hm_get_template_part('parts/media_item');
			hm_get_template_part('parts/media_item');
			hm_get_template_part('parts/media_item');
	*/
			 ?>			
		</div>
		<?php
		if($totalPages > 1){
			$base = get_permalink(get_queried_object())."?pagina=";
		?>
		<div class="pagination">
			<?php
			if($currentPage > 1){

				?>
				<a href='<?php echo $base.($currentPage - 1);?>' class="prev" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></span> 
				<?php
			}
			?>
			

			<?php for($i = 1;$i<$totalPages+1;$i++){
				?>
				<a href='<?php echo $base . $i;?>' class="dot <?php if($i == $currentPage){?>active<?php } ?>"><?php echo $i;?></a>
				<?php	
			}
			?>
			
			
			<?php
			if($currentPage < $totalPages){
				?>
			<a href="<?php echo $base.($currentPage + 1);?>" class="next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></a>
			<?php
		}
			?>
		</div>
		<?php
		}
		?>
		
		
	</div>
</div>

<?php get_footer(); ?>
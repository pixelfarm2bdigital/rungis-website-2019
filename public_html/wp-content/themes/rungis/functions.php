<?php
/**
 * Rungis functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Rungis
 */


setlocale(LC_ALL, 'nl_NL');
if ( ! function_exists( 'rungis_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function rungis_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on HabeonArchitecten, use a find and replace
	 * to change 'rungis' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'rungis', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main' => esc_html__( 'Main', 'rungis' ),
		'footer' => esc_html__( 'Footer-bar', 'rungis' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	/**
	 * Image sizes
	 */

	/*
	hero slider 					- 1440x820 px
	afbeelding in blok Werkwijze 	- 707x519 px
	grote foto Het team 			- 1440x586 px
	afbeelding in blok actueel 		- 390x228 px
	afbeeldingen in collage			- 667x515 px (groot beeld)
									- 348x347 px (vierkant groot)
									- 280x280 px (vierkant klein)
									- 443x280 px (landscape klein)
									- 484x310 px (landscape groot)
	*/
	
	add_image_size('hero_slider', 1440,820, true);
	add_image_size('portrait', 214*2 , 257*2, true);
	//add_image_size('team_photo', 1440, 586, false);

/*
    [0] => thumbnail
    [1] => medium
    [2] => medium_large
    [3] => large
    [4] => hero_slider
    [5] => team_photo


	global $_wp_additional_image_sizes; 
	print '<pre>'; 
	print_r(get_intermediate_image_sizes()); 
	print '</pre>';
	die();
	*/
	if(stripos( $_SERVER['REQUEST_URI'] , "/wp/wp-admin/edit.php?s&post_status=") === 0){
		
		header("location:" .str_replace("?s&", "?", $_SERVER['REQUEST_URI']) );
		die("gfdsgfds");
	}
	
	
}
endif;
add_action( 'after_setup_theme', 'rungis_setup' );

add_filter('gform_confirmation_anchor', '__return_false');

function re_rewrite_rules() {
    global $wp_rewrite;
    // $wp_rewrite->author_base = $author_slug;
//  print_r($wp_rewrite);
    $wp_rewrite->author_base        = 'auteur';
    $wp_rewrite->search_base        = 'zoeken';
    $wp_rewrite->comments_base      = 'reactie';
    $wp_rewrite->pagination_base    = 'pagina';
    $wp_rewrite->flush_rules();
}
add_action('init', 're_rewrite_rules');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function rungis_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'rungis_content_width', 640 );
}
add_action( 'after_setup_theme', 'rungis_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function rungis_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rungis' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'rungis' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'rungis_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function rungis_assets() {
	


	wp_enqueue_style( 'rungis-style', get_template_directory_uri() . '/assets/css/style'.(WP_ENV!='development'?'.min':'').'.css', array(), VERSION);

	wp_enqueue_script( 'rungis-libs', get_template_directory_uri() . '/assets/js/libs'.(WP_ENV!='development'?'.min':'').'.js', array('jquery'), VERSION, true );
	wp_enqueue_script( 'rungis-script', get_template_directory_uri() . '/assets/js/scripts'.(WP_ENV!='development'?'.min':'').'.js', array('rungis-libs'), VERSION, true );

	wp_localize_script( 'rungis-script', 'ajax_object',
  array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );



	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'rungis_assets' );



/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';


/**
 * Custom functions that act independently of the theme templates.
 */

if(WP_ENV != "development"){
	require get_template_directory() . '/inc/acf-fields.php';
}


/**
 * Custom post types
 */	
require get_template_directory() . '/inc/custom-post-types.php';

/**
 * Custom taxonomy
 */
require get_template_directory() . '/inc/custom-taxonomies.php';



/**
 * Humanmade utils
 */
require get_template_directory() . '/inc/hm.php';



require get_template_directory() . '/inc/ajax-products.php';

require get_template_directory() . '/inc/simple_html_dom.php';

require get_template_directory() . '/inc/instagram.php';

require get_template_directory() . '/inc/facebook.php';

require get_template_directory() . '/inc/options-pages.php';


//require get_template_directory(). '/inc/twitteroauth/autoload.php';
//$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token, $access_token_secret);



//require get_template_directory(). '/inc/twitter.php';

//require get_template_directory(). '/inc/instagram.php';

/*  Add responsive container to embeds
/* ------------------------------------ */ 
function alx_embed_html( $html ) {
    return '<div class="video-wrapper">' . 
	str_replace("?", "?rel=0&amp;", $html)
    . '</div>';
}
 
add_filter( 'embed_oembed_html', 'alx_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'alx_embed_html' ); // Jetpack
function get_post_image_src($size = 'large',$post = null){

	if(IMGFIX && get_post_type($post) == "product"){
		return getimagesforproduct()[0];
	}
	$attachmentId = get_post_thumbnail_id($post);
	return wp_get_attachment_image_src( $attachmentId, $size )[0];
}


add_editor_style('assets/css/editor.css');



/**
 * Add All Custom Post Types to search
 *
 * Returns the main $query.
 *
 * @access      public
 * @since       1.0 
 * @return      $query
*/

function rc_add_cpts_to_search($query) {

	// Check to verify it's search page
	if( is_search() && $query->is_main_query() ) {
		// Get post types
		$post_types = get_post_types(array('public' => true, 'exclude_from_search' => false), 'objects');
		$searchable_types = array();
		// Add available post types
		if( $post_types ) {
			foreach( $post_types as $type) {
				$searchable_types[] = $type->name;
			}
		}
		$query->set( 'post_type', $searchable_types );
		$query->set( 'posts_per_page', 8);
	}
	return $query;
}
add_action( 'pre_get_posts', 'rc_add_cpts_to_search' );


function get_url_for_language( $original_url, $language = ICL_LANGUAGE_CODE )
{
    $post_id = url_to_postid( $original_url );
    $lang_post_id = icl_object_id( $post_id , 'page', true, $language );
     
    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink( $lang_post_id );
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url( $language );
    }
     
    return $url;
}


function custom_posts_per_page($query) {


    if (is_home() && $query->is_main_query() ) {

        $query->set('posts_per_page', -1);
    }
    
} //function

//this adds the function above to the 'pre_get_posts' action     
add_action('pre_get_posts', 'custom_posts_per_page');



function limit_text_to_words($text, $limit) {
      if (str_word_count($text, 0) > $limit) {
          $words = str_word_count($text, 2);
          $pos = array_keys($words);
          $text = substr($text, 0, $pos[$limit]) . '...';
      }
      return $text;
    }
<?php
$title = $args['title'];
$image = $args['image'];
$link = $args['link'];
$label = $args['label'];

?>

<a href="<?php echo $link;?>" class="media__item" data-type="video">
	<div class="media__item__inner">
		<div class="media__visual">
			<div style="background-image: url('<?php echo $image;?>');"></div>
		</div>
		<div class="media__content">
			<p><?php echo $label;?></p>
			<p><?php echo $title;?></p>
		</div>
	</div>
</a>
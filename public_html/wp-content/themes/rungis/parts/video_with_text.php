<?php

$title = $args['title'];
$video = $args['video'];
$text = $args['text'];

?>

<div class="video-with-text">
	<div class="inner">
		<div class="row">
			<div class="col col-50">
				<div class="visual">
					<?php hm_get_template_part('parts/video', array('video'=>$video,'size'=>'large')); ?>
				</div>
			</div>
			<div class="col col-50">
				<div class="content">
					<p><?php hm_get_template_part('parts/title', array('title'=>$title,'tag'=>'h2')); ?></p>
					<?php hm_get_template_part('parts/text', array('text'=>$text)); ?>
				</div>
			</div>
		</div>
	</div>
</div>





<?php 
/*
hm_get_template_part('parts/title', array('title'=>$title,'tag'=>'h2'));

hm_get_template_part('parts/video', array('video'=>$video,'size'=>'large'));

hm_get_template_part('parts/text', array('text'=>$text));
*/
?>
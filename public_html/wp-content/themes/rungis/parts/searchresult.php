<?php

if(get_post_type() == "product"){
	$excerpt = strip_tags(get_field("product_intro_text"));
}else{
	$excerpt = get_the_excerpt();
}


if($excerpt == ""){
	$excerpt = strip_tags(get_field("intro_text"));
}

if($excerpt == ""){
	$excerpt = strip_tags(get_field("seizoen_intro_text"));
}


if($excerpt == ""){
	$excerpt = strip_tags(get_field("intro_content"));
}


if($excerpt == ""){
	$excerpt = strip_tags(get_field("intro"));
}


if($excerpt == ""){
	$excerpt = strip_tags(get_field("seizoen_intro_text"));
}




if(get_post_type() == "post"){
	$categories = get_the_category();
}
?>
				<a href="<?php echo get_the_permalink();?>" class="searchresult__item">
					<div class="image is-desktop">
						<div class='image__inner' style='background-image:url(<?php echo get_post_image_src('large' );?> );'>
						</div>
					</div>
					<div class="content">
						<?php
						if(@$categories){
							?><p class="subtitle"><?php echo ($categories[0]->name);?></p><?php
						}?>
						<p class="title"><?php echo get_the_title();?></p>
						<div class="image is-mobile">
							<div class='image__inner' style='background-image:url(<?php echo get_post_image_src('large' );?> );'>
							</div>
						</div>

						<p class='is-desktop'><?php echo $excerpt; ?></p>
						<p class='is-mobile'><?php echo limit_text_to_words($excerpt,16); ?></p>
					</div>
				</a>
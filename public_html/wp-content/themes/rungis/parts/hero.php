<?php if(is_front_page()) { 

	if(CURRENTSEASON == "winter"){
		$seasonKey = "hero";	
	}else{
		if(CURRENTSEASON == "autumn"){
			$seasonKey = "hero_herfst";
		}else{
			$seasonKey = "hero_".CURRENTSEASON;	
		}
		
	}
		

	?>

	<div class="hero hero--large">
		<div class="hero__inner">
			<?php if(have_rows($seasonKey)) : ?>
				<div class="hero__slider">
					<?php while(have_rows($seasonKey)) : the_row(); 
						$image = get_sub_field('hero_afbeelding');
						

						if(is_numeric($image)){
							$theId = $image;
						}elseif(is_array($image)){
							$theId = $image['id'];
						}elseif(is_object($image)){
							$theId = $image->id;
						}elseif(is_string($image)){
							$imageSrc = $image;
						}

						if(is_numeric(@$theId)){
							$imageSrc = wp_get_attachment_image_src( $theId, 'hero_slider', false )[0];
						}


						

						$text = get_sub_field('hero_tekst');
					?>
						
						<div class="hero__item bg-align-vertical-<?php echo get_field("alignment_vertical");?>" style="background-image: url(<?php echo 	$imageSrc; ?>);">
							<div class="hero__item__content">
								<div class='hero__item__content__inner'>
									<?php echo $text; ?>
								</div>
							</div>
						</div>
						
					<?php endwhile; ?>
				</div>
				
				<div class="dots"></div>
			<?php endif; ?>
		</div>
	</div>

<?php } else { ?>

	<?php

	if(is_home()){
			$attachmentId = get_post_thumbnail_id(get_queried_object());
	}else{
		
		$attachmentId = get_post_thumbnail_id();
		
	}

		
		$imageSrc = wp_get_attachment_image_src( $attachmentId, 'hero_slider' );
		if($imageSrc[0]){
			$imageSrc = $imageSrc[0];
		}
	?>

	<div class="hero">
		<div class="hero__inner">
			<div class="hero__inner__media bg-align-vertical-<?php echo get_field("alignment_vertical", get_queried_object());?>" style="background-image: url('<?php echo $imageSrc;?>');"></div>
		</div>
	</div>

<?php } ?>

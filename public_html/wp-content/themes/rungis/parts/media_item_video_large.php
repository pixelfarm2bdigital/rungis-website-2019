<?php
$title = $args['title'];
$image = $args['image'];
$link = $args['link'];

?><a href="<?php echo $link;?>" class="media__item media__item--large" data-type="video-large">
	<div class="media__item__inner">
		<div class="media__visual">
			<div style="background-image: url('<?php echo $image;?>');"></div>
		</div>
		<div class="media__content">
			<div class="media__content__wrapper">
				<p><?php echo __("Gert Jan's keuze","rungis");?></p>
				<p><?php echo $title;?></p>
			</div>
		</div>
	</div>
</a>
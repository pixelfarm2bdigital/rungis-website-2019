<?php

$product = $args['product'];

$categories = get_the_terms($product, "productcategory" );

?>

<a href="<?php echo get_permalink($product); ?>" class="product">
	<div class="product__visual">
		<?php 
		if(IMGFIX){
			hm_get_template_part('parts/image', array('image'=>getimagesforproduct($product)[0],'size'=>'large')); 
		}else{
			hm_get_template_part('parts/image', array('image'=>get_post_thumbnail_id($product),'size'=>'large'));	
		}
		

		?>
	</div>
	<div class="product__content">
		<p class="product__subtitle"><?php echo $categories[0]->name;?></p>
		<?php hm_get_template_part('parts/title', array('title'=>$product->post_title,'tag'=>'p')); ?>
	</div>
</a>





<?php
/*
<a href='<?php echo get_permalink($product);?>'><?php

hm_get_template_part('parts/image', array('image'=>hm_get_post_image_id($product),'size'=>'large'));

hm_get_template_part('parts/title', array('title'=>$product->post_title,'tag'=>'h5'));

?></a>
*/
?>
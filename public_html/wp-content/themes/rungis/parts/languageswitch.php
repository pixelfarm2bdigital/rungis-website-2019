<?php
$languages = icl_get_languages();
if(count($languages) > 1){
?>				
<div class="languages">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
	<?php
		foreach($languages as $lang){

			if($lang['active']) continue;
			?>
			<a href="<?php echo $lang['url'];?>"><img alt="<?php echo $lang['native_name'];?>" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flag-<?php echo $lang['language_code'];?>.png"/></a>
			<?php
		}
	?>
	
	
	<div class="other">
		<?php
		foreach($languages as $lang){
			if(!$lang['active']) continue;
		?>
		<a><img alt="<?php echo $lang['native_name'];?>" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/flag-<?php echo $lang['language_code'];?>.png"/></a>
		<?php
		}
		?>
	</div>
</div>
<?php
}
?>
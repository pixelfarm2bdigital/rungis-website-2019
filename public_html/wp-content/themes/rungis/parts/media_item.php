<?php
$title = $args['title'];
$image = $args['image'];
$link = $args['link'];
$label = @$args['label'];

?><a href="<?php echo $link;?>" class="media__item media__item--small">
	<div class="media__item__inner">
		<div class="media__visual">
			<div style="background-image: url('<?php echo $image;?>');"></div>
		</div>
		<div class="media__content">
			<div class="media__content__inner">
				<p><?php echo $label;?></p>
				<p><?php echo $title;?></p>
			</div>
		</div>
	</div>
</a>
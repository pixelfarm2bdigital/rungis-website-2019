<?php


$queryArgs = array(
	"posts_per_page"=>3
);


if(isset($args['items']) && !empty($args['items'])){
	
	if(is_object($args['items'][0])){
		$items = array();
		foreach($args['items'] as $item){
			$items[] = $item->ID;
		}
		$args['items'] = $items;
	}

	$queryArgs['post_type']= array("post","page","persbericht");
	$queryArgs['post__in'] = $args['items'];
	$queryArgs['orderby'] = 'post__in';
	$queryArgs['posts_per_page'] = 3;
}



if(isset($args['title'])){
	$title = $args['title'];
}else{
	if(isset($queryArgs['post__in'])){
		$title = __("Gerelateerd nieuws","rungis");
	}else{
		$title = __("Laatste nieuws","rungis");	
	}
}

if(isset($args['link'])){
	$link = $args['link'];
}else{
	$link = false;
}

// the query
$the_query = new WP_Query( $queryArgs );

if ( $the_query->have_posts() ) { 
	$count = 0;
?>
<div class="section">
	<h2 class="center"><?php

	if($link){
		?><a href='<?php echo get_url_for_language("/inspiratie/");?>'><?php echo $title;?></a>

		<?php
	}else{
		echo $title;		
	}

	?>
	</h2>
	<div class="news">
		<div class="news__inner">
			<?php while ( $the_query->have_posts() ) : $the_query->the_post(); 
			



			$classes = "";
			if($count++ % 3 == 0){
				$classes .= " news__item--large";
			}
			$link = get_permalink();

			$target = "";
			$label = "";
			if(get_post_type() == "post"){
				$label = get_the_category()[0]->name;	
			}elseif(get_post_type() == "persbericht"){

				if(get_field("persberichten_link")['url']){
					$link = get_field("persberichten_link")['url'];
					
				}elseif(get_field("persberichten_file")){
					$link = get_field("persberichten_file")['url'];
					
				}else{
					$link = get_permalink();
				}
				$target = "_blank";
				$label = __("Publicatie","rungis");
			}
			


			?>
			<a href="<?php echo $link;?>" target="<?php echo $target;?>" class="news__item <?php echo $classes;?>">
	 			<div class="news__item__image">
	 				<div style="background-image: url('<?php echo get_post_image_src();?>');"></div>
	 			</div>
	 			<div class="news__item__content">
	 				<div class="icon">
	 					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
	 				</div>
	 				<p class="news__item__title"><?php echo $label ;?></p>
	 				<p><?php echo get_the_title();?></p>
	 			</div>
	 		</a>
			<?php endwhile; ?>

		</div>
	</div>
</div>
<?php 
}
?>


<?php wp_reset_postdata(); ?>
<?php

$title = $args['title'];
$image = $args['image'];
$text = $args['text'];

?>

<div class="image-with-text">
	<div class="inner">
		<div class="row">
			<div class="col col-50">
			<?php

			
			?>
				<div class="visual" style='background-image:url(<?php echo $image['sizes']['large'];?>);'>

					
				</div>
			</div>
			<div class="col col-50">
				<div class="content">
					<?php hm_get_template_part('parts/title', array('title'=>$title,'tag'=>'h2')); ?>
					<?php hm_get_template_part('parts/text', array('text'=>$text)); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
/*
hm_get_template_part('parts/title', array('title'=>$title,'tag'=>'h2'));

hm_get_template_part('parts/image', array('image'=>$image,'size'=>'large'));

hm_get_template_part('parts/text', array('text'=>$text));
*/
?>
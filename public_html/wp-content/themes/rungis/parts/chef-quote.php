<?php
$name = $args['name'];
$jobtitle = $args['jobtitle'];
$image = $args['image'];
$quote = $args['quote'];
?>
<div class="quote">
	<div class="quote__inner">
		<div class="quote__image" style="background-image: url('<?php echo $image;?>');"></div>
		<div class="quote__content">
			<p><?php echo $quote;?></p>
			<div class="quote__footer">
				<p><?php echo $name;?></p>
				<p><?php echo $jobtitle;?></p>
			</div>
		</div>
	</div>
</div>
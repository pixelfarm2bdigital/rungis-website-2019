
<div class="pagination">
<?php
	global $wp_query;

	$big = 999999999; // need an unlikely integer

	$pagination = paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'next_text'=>'%NEXT%',
		'prev_text'=>'%PREV%'
	//	'type'=>'array'
	) );

	
	$pagination = str_replace("%PREV%", '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>', $pagination);
	$pagination = str_replace("%NEXT%", '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>', $pagination);

	echo $pagination;
?>
</div>
<?php
/*
?>



<div class="pagination">
	<span class="prev"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></span> 
	<span class="dot active">1</span>
	<span class="dot">2</span>
	<span class="dot">3</span>
	<span class="next"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></span>
</div>

<div>
	<pre>
	<span class='page-numbers current'>1</span>
<a class='page-numbers' href='http://rungis.falcon.pxlfrm.com/inspiratie/page/2/'>2</a>
<a class='page-numbers' href='http://rungis.falcon.pxlfrm.com/inspiratie/page/3/'>3</a>
<a class="next page-numbers" href="http://rungis.falcon.pxlfrm.com/inspiratie/page/2/">Volgende &raquo;</a></div>	</div>
</div>



</div>
*/
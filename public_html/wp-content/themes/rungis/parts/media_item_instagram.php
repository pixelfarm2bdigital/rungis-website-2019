<?php
	$image = $args['image'];
?><div class="media__item" data-type="instagram">
	<div class="media__item__inner">
		
		<div class="icon">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 458.9 458.9"><path d="M404.8 0H55.7C25.7 0 0 28.3 0 58.2V407.5c0 30 25.7 51.4 55.7 51.4H404.8c30.1 0 54.1-21.4 54.1-51.4V58.2C458.9 28.3 434.8 0 404.8 0zM394 65.6c8.7 0 15.8 7.1 15.8 15.8v50.4c0 8.7-7.1 15.8-15.8 15.8h-50.4c-8.7 0-15.8-7.1-15.8-15.8V81.3c0-8.7 7.1-15.8 15.8-15.8H394zM230.8 145.6c48.6 0 88.1 39.3 88.1 87.8 0 48.5-39.4 87.8-88 87.8 -48.6 0-88-39.3-88-87.8C142.7 184.9 182.1 145.6 230.8 145.6zM409.7 395.8c0 7-7.5 13.9-14.6 13.9H66.4c-7.1 0-17.2-6.9-17.2-13.9V196.7h47.3c-3.2 16.4-5 24-5 36.7 0 76.6 62.5 138.9 139.3 138.9 76.8 0 139.3-62.3 139.3-138.9 0-12.7-1.7-20.3-4.9-36.7h44.6V395.8z"/></svg>
		</div>

		<div class="media__visual">
			<?php echo do_shortcode('[instagram-feed num=1] '); ?>
		</div>
	</div>
</div>			
<style>
	.sbi_info{
		display:none;
	}
	#sbi_load{
		display:none;
	}
</style>
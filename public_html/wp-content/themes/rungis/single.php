<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Rungis
 */

get_header(); 
$introTitle = get_field("intro_title");
$introText = get_field("intro_text");
$contentblocks = get_field('content');
$formShow = get_field('form_show');
$relatedProducts = get_field('products_products');
$relatedArticles = get_field('inspiration_related');
?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">

	<?php // hardcoded content ?>
	<div class="intro">
		<div class="intro__inner">
			<div class="tags">
				
				<?php 
				$categories = get_the_category();
				if(is_array($categories) && !empty($categories)){
					foreach($categories as $c){
						?>
						<a class="tag"><?php echo $c->name;?></a>
						<?php
					}
				}
				?>
			</div>
			<h1 class="intro__title"><?php echo $introTitle;?></h1>
			<?php echo $introText;?>
		</div>
	</div>

	<?php if(is_array($contentblocks)){ ?>
	<div class="wrap--small cms-content">
		<?php foreach($contentblocks as $key => $block) {
			$type = $block['acf_fc_layout'];
		?>
		
		<?php
			if($type == "slider"){
				hm_get_template_part('parts/slider', array('slider'=>$block['afbeeldingen']));

			}elseif($type == "text"){
				hm_get_template_part('parts/text', array('text'=>$block['text']));

			}elseif($type == "quote"){
				hm_get_template_part('parts/quote', array('quote'=>$block['quote']));

			}elseif($type == "photo_with_text"){
				hm_get_template_part('parts/photo_with_text', array('title'=>$block['title'], 'image'=>$block['image'], 'text'=>$block['text']));
			}elseif($type == "video_with_text"){
				hm_get_template_part('parts/video_with_text', array('title'=>$block['title'], 'video'=>$block['video'], 'text'=>$block['text']));
			}
			/*
			slider
			text
			quote
			photo_with_text
			video_with_text
			*/
			
			//hm_get_template_part('parts/slider-with-text', array('slider'=>$slider, 'title'=>false, 'text'=> $block['text'], 'button'=>false, 'label'=>false, 'labelClass'=>'labelRight', 'sliderPosition'=>'left'));

		?>
	
	
		<?php 
		} ?>
	</div> 

	<?php } ?>

	

	<?php
	if($formShow){
		$formTitle = get_field("form_title");
		$formText = get_field("form_text");
		$formForm = get_field("form_form");
		
		/*
		echo '<div class="formwrapper">';
		echo '<div class="formwrapper__inner">';
		hm_get_template_part('parts/title', array('title'=>$formTitle,'tag'=>'h2'));
		hm_get_template_part('parts/text', array('text'=>$formText));
		hm_get_template_part('parts/form', array('form'=>$formForm));
		echo '</div>';
		echo '</div>';
		*/
		
		echo '<div class="formwrapper contactform">';
		echo '<div class="formwrapper__inner">';
		hm_get_template_part('parts/title', array('title'=>$formTitle,'tag'=>'h2'));
		hm_get_template_part('parts/text', array('text'=>$formText));
    	$form_object = $formForm;
    	echo do_shortcode('[gravityform id="' . $form_object['id'] . '" title="false" description="false" ajax="true"]');
    	echo '</div>';
		echo '</div>';
	}

	?>
	<?php hm_get_template_part('parts/socialshare');  ?>	
	
	<?php
	if($relatedProducts && !empty($relatedProducts)) {
	?>
	
	<div class="section">
		<!-- hardcoded title -->
		<h2 class="center"><?php echo __("Gerelateerde producten","rungis");?></h2>
	
		<div class="products">
			<?php foreach($relatedProducts as $product) { ?>
				<?php hm_get_template_part('parts/product', array('product'=>$product)); ?>
			<?php } ?>
		</div>
	
		<?php
		/*
		<ul>
			<?php foreach($relatedProducts as $product) { ?>
				<li>
					<?php hm_get_template_part('parts/product', array('product'=>$product)); ?>
				</li>
			<?php } ?>
		</ul>
		*/
		?>
	</div>
	<?php } ?>


	<?php
	if($relatedArticles && !empty($relatedArticles)){
	
	?>

	<?php hm_get_template_part('parts/news',array("items"=>get_field("inspiration_related")));  ?>
	<?php
	}	
	?>
	
	
</div>


<?php get_footer(); ?>
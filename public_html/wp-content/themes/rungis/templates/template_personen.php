<?php
/*
 * Template Name: Personen
 */
get_header();?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo the_title(); ?></h1>
			<?= get_post_field('post_content', $post->ID); ?>
		</div>
	</div>
	
	<div class="personenwrapper">
		<div class="personen">
			<?php
				$personen = new WP_Query(array('post_type' => 'persoon', 'posts_per_page' => -1));
				while($personen->have_posts()) : $personen->the_post();
					$image = get_field('persoon_image');
					$jobtitle = get_field('persoon_title');
					$quote = get_field('persoon_quote');
					$email = get_field('persoon_email');
					
					$attachmentId = get_post_thumbnail_id();
					$imageSrc = wp_get_attachment_image_src( $attachmentId, 'large' );
					if($imageSrc[0]){
						$imageSrc = $imageSrc[0];
					}
			?>
			
				<div class="persoon">
					<div class="persoon__inner">
						<div class="persoon__image" style="background-image: url(<?php echo $imageSrc; ?>);">
							<?php if($email) { ?>
								<a href="mailto:<?php echo $email ?>" class="persoon__email">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="22" height="16" viewBox="0 0 22 16">
  										<image id="Vector_Smart_Object" data-name="Vector Smart Object" width="22" height="16" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAQCAQAAAC45EetAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgDAIMMB0AMzfmAAAA4klEQVQoz5XSoVICURSH8W+vDIFC2uI4m/ctSGrErg9gJ1rhFcgWjVSCAQJJCl1nCO7gDCQKgRnnMzjgrqvIntvO/1fOOTfyjTOOqyxSYMHTP/CcU0Cdu7QjB17HpXNFHZua2f2Tds1MHe8wJr7YN5RgsO+rieQxxs58sF6gdR+dGcsXDvsRVrRIGNDYdxoMSGix2jVCbuI1lwSGNAFoMuSEC9bfIBQWtOGKBSNiYka802aTj4sYttzwzIQJU67ZFsNaaf0f3HJHRA9/RmUM0Pv9jIEKVQnXgJT7I2QKUZUv+gmBq5R86FRYPAAAAABJRU5ErkJggg=="/>
									</svg>
								</a>
							<?php } ?>
						</div>
						<?php
							echo the_title();
							echo '<p class="persoon__title">'.$jobtitle.'</p>';
							echo '<p class="persoon__quote">'.$quote.'</p>';
						?>					
					</div>
				</div>
			
			<?php
				endwhile;
				wp_reset_query();
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
<?php
/*
 * Template Name: Homepage
 */
get_header(); ?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="row firstrow">
		<div class="media slider">

			<?php

			/*
			Smaak van de chef
			*/
			$posts = get_posts( 
				array(
					"category_name"=>"smaak",
					"posts_per_page"=>1
				)
			);
			$thisPost = (current($posts));
			$label = get_the_category($thisPost)[0]->name;
			hm_get_template_part('parts/media_item_large',
				array(
					'title' => get_the_title($thisPost),
					'link'  => get_permalink($thisPost),
					'image' => get_post_image_src('large',$thisPost->ID),
					'label' => $label
				)
			);


			/*
			Gertjans keuze
			*/
			$posts = get_posts( 
				array(
					"category_name"=>"video",
					"posts_per_page"=>1
				)
			);
			$thisPost = (current($posts));
			$label = get_the_category($thisPost)[0]->name;
			$thumbnail = get_field("thumbnail_image",$thisPost);

			if($thumbnail){
				$image = $thumbnail['sizes']['large'];
			}else{
				$image = get_post_image_src('large',$thisPost->ID);
			}


			hm_get_template_part('parts/media_item_video',
				array(
					'title' => get_the_title($thisPost),
					'link'  => get_permalink($thisPost),
					'image' => $image,
					'label' => $label
				)
			);
			
			$instagramPost = getInstPost();

			hm_get_template_part('parts/media_item_instagram',array("image"=>$instagramPost));

			$fbpost = getFBpost();
			if($fbpost){
				hm_get_template_part('parts/media_item_facebook',$fbpost);
			}
			
			/*
			hm_get_template_part('parts/media_item_facebook',array(
				"image"=>"https://scontent.xx.fbcdn.net/v/t1.0-9/15492246_1391596200859536_5659437045311498338_n.jpg?oh=e94f1e2f6939015f529e6f69d5f75dcb&oe=58F38185",
				"text"=>"Rainier kersen. Gele kersen. Een bijzonder zoete smaak. Gebruik rauw of als compote. Zet de #seizoenen op de kaart!",
				"likes"=>9
				)
			);
			*/
			//https://scontent.xx.fbcdn.net/v/t1.0-9/15492246_1391596200859536_5659437045311498338_n.jpg?oh=e94f1e2f6939015f529e6f69d5f75dcb&oe=58F38185


			?>
		</div>
	</div>
	
	<div class="section">
		<h2 class="center"><a href='<?php echo get_url_for_language("/5-seizoenen/");?>'><?php echo __("De vijf seizoenen van Rungis","rungis");?></a></h2>
		<div class="tabswrapper">
			<?php

			$seasons = array();
			$seasons['winter'] = array(
				"title"=>__("Winter","rungis"),
				"rawtitle"=>"Winter",
				"slogan"=>get_field("season_winter_title"),
				"text"=>get_field("season_winter_text"),
				"categories"=>get_field("season_winter_categories")
			);
			$seasons['spring'] = array(
				"title"=>__("Lente","rungis"),
				"rawtitle"=>"Lente",
				"slogan"=>get_field("season_spring_title"),
				"text"=>get_field("season_spring_text"),
				"categories"=>get_field("season_spring_categories")
			);
			$seasons['presummer'] = array(
				"title"=>__("Voorzomer","rungis"),
				"rawtitle"=>"Voorzomer",
				"slogan"=>get_field("season_presummer_title"),
				"text"=>get_field("season_presummer_text"),
				"categories"=>get_field("season_presummer_categories")
			);
			$seasons['summer'] = array(
				"title"=>__("Zomer","rungis"),
				"rawtitle"=>"Zomer",
				"slogan"=>get_field("season_summer_title"),
				"text"=>get_field("season_summer_text"),
				"categories"=>get_field("season_summer_categories")
			);
			$seasons['autumn'] = array(
				"title"=>__("Herfst","rungis"),
				"rawtitle"=>"Herfst",
				"slogan"=>get_field("season_autumn_title"),
				"text"=>get_field("season_autumn_text"),
				"categories"=>get_field("season_autumn_categories")
			);

		
			?>
			<?php // triggers ?>

			<?php
			  $currentSeason = CURRENTSEASON;
			?>

			<?php foreach($seasons as $key=> $season){ ?>
				<input id="<?php echo $key;?>" type="radio" name="tabs-seasons" <?php if($key === $currentSeason) {?>checked <?php } ?>/>
				<label for="<?php echo $key;?>" data-season="<?php echo $key;?>"><?php echo $season['title'];?></label>
				
			<?php } ?>
			
			<?php //tabs ?>
			<?php

			foreach($seasons as $key=> $season) {

				?>
				<div id="tab-<?php echo $key;?>" class="tab__content" data-season="<?php echo $key;?>">
					<div class="tab__content__text">
						<p class="tab__title"><?php echo $season['slogan'];?></p>
						<p>
						<?php echo $season['text'];?>
						</p>
						<div class="buttons">
							<a href="<?php echo get_url_for_language("/seizoenen/");?>#<?php echo strtolower($season['rawtitle']);?>" class="btn btn--pri">
								<?php echo __("Het ".strtolower($season['rawtitle'])."seizoen","rungis");?>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
							</a>
							<a href="/producten/#/categorie/alle/seizoen/<?php echo strtolower($season['title']);?>/bio/alle" class="btn btn--sec">
								<?php echo __("".$season['rawtitle']."producten","rungis");?>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
							</a>
							<a href="/seizoenen/" class="btn btn--tert is-mobile">
								<?php echo __("Bekijk alle seizoenen","rungis");?>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
							</a>
						</div>
					</div>
					<div class="portals">
						<?php if(!$season['categories']){
							echo __("Geen categorie beschikbaar","rungis");
						}else{ ?>
							<?php foreach($season['categories'] as $c){
								
								?>
								<a href="/producten/#/categorie/<?php echo $c['category']->slug;?>/seizoen/<?php echo strtolower($season['rawtitle']);?>/bio/alle" class="portal">
									<div class="portal__inner">
										<div class="portal__image">
											<?php 
											$image = $c['image'];
											if(is_numeric($image)){
												$image = wp_get_attachment_image_src( $image, 'large' );
												if($image[0]){
													$image = $image[0];
												}
											}else{
												$image = $image['sizes']['large'];
											}
											?>
											<div style="background-image: url(<?php echo $image;?>);"></div>
										</div>
										<div class="portal__content"><?php echo $c['category']->name;?> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></div>
									</div>
								</a>
								<?php
							}
						}
						?>
					</div>
				</div>
				

				<?php
			}
			?>
		</div>		
	</div>
	
	<?php hm_get_template_part('parts/news',
		array(
			"title"=>__("Op de kaart","rungis"),
			"link"=>true
		)
	);  ?>
</div>


<?php get_footer(); ?>
<?php
/*
 * Template Name: Producten
 */
get_header() ?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap" ng-app="rungisApp" ng-controller="ProductsController as vm" >
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo the_title(); ?></h1>
			<?= get_post_field('post_content', $post->ID); ?>
		</div>
	</div>

	<div class="producten pending" ng-class="{'pending': !!vm.pending }">
		<div class="producten__header">
			<div class="row">
				<div class="col col-25">
					<span class="title"><?php echo __("Filter","rungis");?></span>

				</div>
				<div class="col col-50">
					<span class="total"><span ng-class="{'loaded':vm.dirty}"><span ng-bind="vm.products.length"></span> <?php echo __("producten gevonden","rungis");?></span></span>
				</div>
				<div class="col col-25 tr">
					<div class="selectwrapper">
						<select ng-model="vm.pageSize" ng-change="vm.pageNr = 1;">
							<option value="24">24 <?php echo __("producten","rungis");?></option>
							<option value="48">48 <?php echo __("producten","rungis");?></option>
							<option value="99999999"><?php echo __("Alle producten","rungis");?></option>
						</select>
					</div>
				</div>
			</div>			
		</div>

		<div class='producten__headermobile'>
			<a class='btn btn--pri is-mobile' ng-click="vm.toggleMobile();"><?php echo __("Toon filter","rungis");?></a>
		</div>
		
		<div class="producten__content">
			<div class="row">
				<div class="col col-25">
					
					<?php
					$productCategories = get_terms( array(
					    'taxonomy' => 'productcategory',
					    'hide_empty' => false,
					) );


					$rawCats = array();
					foreach($productCategories as $productCat){
						if($productCat->parent == 0){
							$productCat->sub = array();
							$productCat->is_sub = false;
							$rawCats[$productCat->term_id] = $productCat;
						}
					}
					foreach($productCategories as $productCat){
						if($productCat->parent != 0){
							$productCat->is_sub = true;
							$rawCats[$productCat->parent]->sub[] = $productCat;
						}
					}


					function cmp($a, $b){
						return strcmp($a->name, $b->name);
					}
					usort($rawCats, 'cmp');

					

					/*

					$productCategories = array();
					foreach($rawCats as $c){
						$copy = clone $c;
						unset($c->sub);
						$productCategories[] = $c;
						if(isset($copy->sub)){
							foreach($copy->sub as $subcat){
								$productCategories[] = $subcat;
							}
						}
					}
					*/
					
					?>
					
					<script> window.productCategories = <?php echo json_encode($rawCats);?>; </script>
					
					<p class="title"><?php echo __("Seizoenen","rungis");?></p>
					<ul class="list list--products list--products__desktop" ng-repeat="season in vm.seasons">
						<li class='subcat'>
							<a ng-class='{"active" : vm.selectedSeasons.indexOf(season.slug) > -1}' ng-click="vm.toggleSeason(season.slug); vm.updateFilter();">{{season.name}}</a>
						</li>
					</ul>

					<p class="title"><?php echo __("Biologisch","rungis");?></p>
					<ul class="list list--products list--products__desktop">
						<li class='subcat'>
							<a ng-class='{"active" : vm.selectedBio == "alleen-bio"}' ng-click="vm.toggleBio(); vm.updateFilter();"><?php echo __("Alleen biologisch","rungis");?></a>
						</li>
					</ul>

					<p class="title"><?php echo __("Categorie","rungis");?></p>
					
					<ul class="list list--products list--products__desktop">
						<li ng-repeat="category in vm.categories" class='maincat' ng-class='{"sub":category.is_sub,"active":category.is_open}'>
							<a ng-click="vm.toggleMainCat(category);">{{category.name}}</a>
							<ul ng-if="category.sub" >
								<li class='subcat check-all'>
									<a ng-if="!vm.getSubCatsStatus(category);" ng-click="vm.checkAllForMainCat(category,true);"><?php echo __("Alles selecteren","rungis");?></a>
									<a  class='active' ng-if="vm.getSubCatsStatus(category);" ng-click="vm.checkAllForMainCat(category,false);"><?php echo __("Alles de-selecteren");?></a>
								</li>
								<li class='subcat' ng-repeat="category in category.sub">
									<a  ng-class='{"active" : vm.selectedCats.indexOf(category.slug) > -1}' ng-click="vm.toggleCat(category.slug); vm.updateFilter();">{{category.name}}</a>
								</li>
							</ul>
						</li>
					</ul>
					
				</div>
				<div class="col col-75">
					<div class="productenwrapper">						
						<a ng-repeat="product in vm.products | orderByPriority" ng-if="$index >= (vm.pageSize * (vm.pageNr - 1)) && $index < (vm.pageSize * vm.pageNr)" ng-href="{{product.permalink}}" class="product">
							<div class="product__inner">						
								<div class="product__media" ng-if="product.image" ng-style="{'background-image':'url({{product.image}})'}"></div>
								<div class="product__content">
									<p class="product__category" ng-bind="product.category"></p>
									<span>{{product.title}}</span>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="pagination" ng-if="vm.totalPages > 1">
		
		<span class="prev" ng-click="vm.pageNr = (vm.pageNr > 1?vm.pageNr - 1:vm.pageNr);vm.scrollToTop();" ><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></span> 

		<span ng-repeat="p in [] | range:vm.totalPages" class="dot" ng-click="vm.pageNr = (p+1);vm.scrollToTop();" ng-class="{'active':vm.pageNr == p + 1}">{{p + 1}}</span>
		
		
		<span class="next" ng-click="vm.pageNr = (vm.pageNr < vm.totalPages?vm.pageNr + 1:vm.pageNr);vm.scrollToTop();"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></span>
	</div>
	<div class="list list--products list--products__mobile" ng-class="{'active':vm.mobileFilter,'pending': !!vm.pending }"'>
		<div class='scrollpane'>
			
			<p class="title"><?php echo __("Seizoenen","rungis");?></p>
			<ul class="list list--products " ng-repeat="season in vm.seasons">
				<li class='subcat'>
					<a ng-class='{"active" : vm.selectedSeasons.indexOf(season.slug) > -1}' ng-click="vm.toggleSeason(season.slug); vm.updateFilter();">{{season.name}}</a>
				</li>
			</ul>

			<p class="title"><?php echo __("Biologisch","rungis");?></p>
			<ul class="list list--products">
				<li class='subcat'>
					<a ng-class='{"active" : vm.selectedBio == "alleen-bio"}' ng-click="vm.toggleBio(); vm.updateFilter();"><?php echo __("Alleen biologisch","rungis");?></a>
				</li>
			</ul>


			<p class="title"><?php echo __("Categorie","rungis");?></p>
			
			<ul class="list list--products">
				<li ng-repeat="category in vm.categories" class='maincat' ng-class='{"sub":category.is_sub,"active":category.is_open}'>
					<a ng-click="vm.toggleMainCat(category);">{{category.name}}</a>
					<ul ng-if="category.sub" >
						<li class='subcat check-all'>
							<a ng-if="!vm.getSubCatsStatus(category);" ng-click="vm.checkAllForMainCat(category,true);"><?php echo __("Alles selecteren","rungis");?></a>
							<a  class='active' ng-if="vm.getSubCatsStatus(category);" ng-click="vm.checkAllForMainCat(category,false);"><?php echo __("Alles de-selecteren","rungis");?></a>
						</li>
						<li class='subcat' ng-repeat="category in category.sub">
							<a  ng-class='{"active" : vm.selectedCats.indexOf(category.slug) > -1}' ng-click="vm.toggleCat(category.slug); vm.updateFilter();">{{category.name}}</a>
						</li>
					</ul>
				</li>
			</ul>
			
			
		</div>
		<div class='btn-container'>
			<a class='btn btn--pri' ng-click="vm.toggleMobile();"><span class="" ng-if="vm.pending">Laden...</span><span ng-if='!vm.pending'>Toon <span ng-bind="vm.products.length"></span> producten</span></a>
		</div>
	</div>
</div>



<?php get_footer(); ?>
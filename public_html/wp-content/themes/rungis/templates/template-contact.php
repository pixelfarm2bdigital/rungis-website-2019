<?php
/*
 * Template Name: Contact
 */
get_header(); 

$introTitle = get_field('intro_title');
$introText = get_field('intro_content');

$contact = get_field('contact_gegevens');
$contactAdres = get_field('contact_adres');
$contactExtra = get_field('contact_extra');

$formShow = get_field('form_show');

$googleMap = get_field('google_map');
?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo $introTitle; ?></h1>
			<?php echo $introText; ?>
		</div>
	</div>
	
	<div class="contactwrapper">
		<div class="row">
			<div class="col col-50">
				<div class="pod pod--data">				
					<div class="phone-and-email">
						<?php echo $contact; ?>
					</div>
				</div>
				<div class="pod pod--data pod--data__small">
					<div class="address">
						<?php echo $contactAdres; ?>
					</div>
				</div>
				<div class="pod">
					<?php echo $contactExtra; ?>
				</div>
			</div>
			<div class="col col-50">				
				<div id="map" class="map js-map" data-lat="<?php echo $googleMap['lat']; ?>" data-lng="<?php echo $googleMap['lng']; ?>"></div>
				
				<a href="https://www.google.nl/maps/dir//Rungis+B.V.,+Transportweg+34,+2991+LV+Barendrecht/" target="_blank" class="btn btn--pri">
					<?php echo __("Route plannen","rungis");?>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg>
				</a>
			</div>
		</div>
	</div>
	
	<?php
	if($formShow){
		
		$formTitle = get_field("form_title");
		$formText = get_field("form_text");
		$formForm = get_field("form_form");
		
		/*
		echo '<div class="formwrapper">';
		echo '<div class="formwrapper__inner">';
		hm_get_template_part('parts/title', array('title'=>$formTitle,'tag'=>'h2'));
		hm_get_template_part('parts/text', array('text'=>$formText));
		hm_get_template_part('parts/form', array('form'=>$formForm));
		echo '</div>';
		echo '</div>';
		*/
		
		echo '<div class="formwrapper contactform">';
		echo '<div class="formwrapper__inner">';
		hm_get_template_part('parts/title', array('title'=>$formTitle,'tag'=>'')); 
		hm_get_template_part('parts/text', array('text'=>$formText));
    	$form_object = $formForm;
    	echo do_shortcode('[gravityform id="' . $form_object['id'] . '" title="false" description="false" ajax="true" tabindex="10"]');
    	echo '</div>';
		echo '</div>';
		
	}
	?>
</div>

<?php get_footer(); ?>
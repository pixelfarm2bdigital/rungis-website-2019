<?php
/*
 * Template Name: Onze geschiedenis
 */
get_header();

$introTitle = get_field('intro_title');
$introText = get_field('intro_text');
?>

<?php hm_get_template_part('parts/hero'); ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo $introTitle; ?></h1>
			<?php echo $introText; ?>
		</div>
	</div>
	
	<?php if(have_rows('hoogtepunt')) : ?>

		<?php
		
		$hoogtepunten = get_field("hoogtepunt");
		

		?>
		<div class="timeline">
			<div class="timeline__inner">
				<div class="line"></div>

				<?php
				$cm = false;
				foreach($hoogtepunten as $key=> $hoogtepunt){
					$date = $hoogtepunt['hoogtepunt_date'];
					$image = $hoogtepunt['hoogtepunt_image'];
					$title = $hoogtepunt['hoogtepunt_title'];
					$text = $hoogtepunt['hoogtepunt_text'];

					$year = preg_match("/(\d{4})/", $date,$matches);
					if($year){
						$year = $matches[1];
					}
					
					$century = floor($year / 10)*10;
					

					//$year = 

					if($cm != $century){
						$cm = $century;
						?>
					<div class="line__label line__label<?php echo $key;?>"><span><?php echo $century;?></span></div>
						<?php
					}

					?>

					<div class="timeline__item <?php echo ($key % 2 == 0?"odd":"even");?> ">
						<div class="timeline__item__date-and-image">
							<span class="date"><?php echo $date; ?></span>
							<div class="image">
								<div style="background-image: url(<?php echo $image['sizes']['large']; ?>);"></div>
							</div>
						</div>
						<div class="timeline__item__content">
							<p class="timeline__item__date"><?php echo $date; ?></p>
							<p class="timeline__item__title"><?php echo $title; ?></p>
							<?php echo $text; ?>
						</div>
					</div>


					<?php



				}
					
				?>
				
					
				
				
			</div>
		</div>
	<?php endif; ?>
</div>

<?php get_footer(); ?>
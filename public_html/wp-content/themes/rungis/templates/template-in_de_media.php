<?php
/*
 * Template Name: In de media
 */
get_header() ?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo the_title(); ?></h1>
			<?= get_post_field('post_content', $post->ID); ?>
		</div>
	</div>
	
	<div class="media">
		<div class="media__filter mt">
			<div class="media__filter__item">
				<input id="all" type="checkbox" name="a" <?php if( 

				!isset($_GET['type']) 

				){ ?>checked<?php } ?> /> 
				<label for="all"><?php echo __("Alle categorie&euml;n","rungis");?></label>
			</div>
			<div class="media__filter__item">
				<input id="digitale" value="digitale-media" type="checkbox" name="media" <?php if(

				(isset($_GET['type']) && stristr($_GET['type'],"digitale-media") )
				|| 
				(!isset($_GET['type']))

				){ ?>checked<?php } ?>/> 
				<label for="digitale"><?php echo __("Digitale media","rungis");?></label>
			</div>
			<div class="media__filter__item">
				<input id="vakbladen" value="vakbladen" type="checkbox" name="media" <?php if(

				(isset($_GET['type']) && stristr($_GET['type'],"vakbladen"))
				|| 
				(!isset($_GET['type']))

				){ ?>checked<?php } ?>/> 
				<label for="vakbladen"><?php echo __("Vakbladen","rungis");?></label>
			</div>
			<div class="media__filter__item">
				<input id="kranten" value="kranten-en-tijdschriften" type="checkbox" name="media" <?php if(

				(isset($_GET['type']) && stristr($_GET['type'],"kranten-en-tijdschriften"))
				|| 
				(!isset($_GET['type']))

				){ ?>checked<?php } ?>/> 
				<label for="kranten"><?php echo __("Kranten en tijdschriften","rungis");?></label>
			</div>
		</div>
		
		<div class="persberichten">
			<?php

			$args = array(
				'post_type' => 'persbericht', 
				'posts_per_page' => -1
			);

			if(isset($_GET['type'])){
				$terms = explode(",", $_GET['type']);
				$args['tax_query'] = array(
		            array(
		                'taxonomy' => 'publicatietype',
		                'field' => 'slug',
		                'terms' => $terms
		            ),
		        );
			}

			$persbericht = new WP_Query(
				$args
			);
			while($persbericht->have_posts()) : $persbericht->the_post();
				//$image = get_field('persoon_image');
				
				$attachmentId = get_post_thumbnail_id();
				$category = get_the_terms( null, "publicatietype");

				$imageSrc = wp_get_attachment_image_src( $attachmentId, 'hero' );
				if($imageSrc[0]){
					$imageSrc = $imageSrc[0];
				}

				if(get_field("persberichten_link")['url']){
					$link = get_field("persberichten_link")['url'];
					
				}elseif(get_field("persberichten_file")){
					$link = get_field("persberichten_file")['url'];
					
				}else{
					$link = get_permalink();
				}

			?>
			
				<a href="<?php echo $link;?>" target="_blank" class="persberichten__item">
					<div class="persberichten__item__inner">						
						<div class="persberichten__item__media">
							<div style="background-image: url(<?php echo $imageSrc;?>);"></div>
						</div>
						<div class="persberichten__item__content">
							<p class="persberichten__item__content__header">
								<span class="date"><?php echo get_the_date('M Y'); ?></span>
								<span class="category"><?php echo @$category[0]->name;?></span>
							</p>
							<?php echo the_title(); ?>
							<!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="14" viewBox="0 0 8 14">
  								<image id="Vector_Smart_Object_copy_16" data-name="Vector Smart Object copy 16" y="-1" width="8" height="15" xlink:href="data:img/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAPCAMAAADu1H4BAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAANlBMVEX///+oAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFaoAFYAAADvINVlAAAAEHRSTlMAB+2wBqGyA6WgAZeflqPnXKrwogAAAAFiS0dEEeK1PboAAAAJcEhZcwAACxIAAAsSAdLdfvwAAAAHdElNRQfgDAIOLBXrGzbnAAAAMklEQVQI12NgZIACJmYWCINVgA3CYufAwuKEsbi4kVg8MGW8UJqDHQ/NwAelGfj5IDQAgw0CO83BWI0AAAAASUVORK5CYII="/>
							</svg> -->
						</div>
					</div>
				</a>
			
			<?php
				endwhile;
				wp_reset_query();
			?>
		</div>
	</div>
</div>

<?php get_footer(); ?>
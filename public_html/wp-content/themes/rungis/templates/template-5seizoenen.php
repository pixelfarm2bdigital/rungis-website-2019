<?php
/*
 * Template Name: 5 seizoenen
 */
get_header();

$introTitle = get_field('seizoen_intro_title');
$introText = get_field('seizoen_intro_text');
?>

<?php hm_get_template_part('parts/hero');  ?>

<div class="wrap">
	<div class="intro">
		<div class="intro__inner">
			<h1><?php echo $introTitle; ?></h1>
			<?php echo $introText; ?>
		</div>
	</div>
	
	<?php if(have_rows('seizoen')) : 

	$seasonKeys = array(
		"winter",
		"lente",
		"voorzomer",
		"zomer",
		"herfst"
	);
	?>
		<div class="seasons slider">
			<?php while(have_rows('seizoen')) : the_row();
				$seizoenType = get_sub_field('seizoen_type');
				$seizoenImage = get_sub_field('seizoen_image');
				$seizoenTitle = get_sub_field('seizoen_title');
				$seizoenText = get_sub_field('seizoen_text');
				$seizoenLinkUrl = get_sub_field('seizoen_link')['url'];
				$seizoenLinkText = get_sub_field('seizoen_link')['title'];
				$key = array_shift($seasonKeys);
			?>
				<div class="season <?php echo ucfirst($key); ?>">
					<div class="season__image">
						<div style="background-image: url(<?php echo $seizoenImage['url']; ?>);"></div>
					</div>
					<div class="season__content">
						<div class='season__anchor' id="<?php echo $key;?>"></div>
						<div class="season__content__inner">
							<p class="season__title"><?php echo $seizoenTitle; ?></p>
							
							<?php echo $seizoenText; ?>							
							
							<?php
								if($seizoenLinkText) {
									echo '<a href="'.$seizoenLinkUrl.'" class="season__btn">'.$seizoenLinkText.'<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></a>';
								}
							?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	
	<?php // hardcoded banner ?>
	<div class="banner banner--pri">
		<div class="price">&euro;&nbsp;34,<span>95</span></div>
		<img src="<?php echo get_template_directory_uri(); ?>/assets/img/inspiratieboek.png" class="boek"/>
		<div class="banner__inner">
			<div class="banner__content">
				<p class="banner__title"><?php echo get_field("book_title");?></p>
				<p><?php echo get_field("book_text");?></p>
				<?php 
				$order = get_field("book_order");
				$preview = get_field("book_preview");

				?>
				<a href="<?php echo $order['url'];?>" target="<?php echo $order['target'];?>" class="banner__btn"><?php echo $order['title'];?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></a>
				<a href="<?php echo $preview['url'];?>" target="<?php echo $preview['target'];?>" class="banner__btn"><?php echo $preview['title'];?><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 477.2 477.2"><path d="M360.7 229.1l-225.1-225.1c-5.3-5.3-13.8-5.3-19.1 0s-5.3 13.8 0 19.1l215.5 215.5 -215.5 215.5c-5.3 5.3-5.3 13.8 0 19.1 2.6 2.6 6.1 4 9.5 4 3.4 0 6.9-1.3 9.5-4l225.1-225.1C365.9 242.9 365.9 234.3 360.7 229.1z"/></svg></a>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
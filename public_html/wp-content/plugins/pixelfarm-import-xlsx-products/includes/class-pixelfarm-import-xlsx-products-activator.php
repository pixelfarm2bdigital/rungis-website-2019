<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.pixelfarm.nl
 * @since      1.0.0
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/includes
 * @author     Pixelfarm <getintouch@pixelfarm.nl>
 */
class Pixelfarm_Import_Xlsx_Products_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}

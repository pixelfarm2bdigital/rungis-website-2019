<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.pixelfarm.nl
 * @since             1.0.0
 * @package           Pixelfarm_Import_Xlsx_Products
 *
 * @wordpress-plugin
 * Plugin Name:       Import XLS products
 * Plugin URI:        https://www.pixelfarm.nl
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Pixelfarm
 * Author URI:        https://www.pixelfarm.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pixelfarm-import-xlsx-products
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pixelfarm-import-xlsx-products-activator.php
 */
function activate_pixelfarm_import_xlsx_products() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pixelfarm-import-xlsx-products-activator.php';
	Pixelfarm_Import_Xlsx_Products_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pixelfarm-import-xlsx-products-deactivator.php
 */
function deactivate_pixelfarm_import_xlsx_products() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pixelfarm-import-xlsx-products-deactivator.php';
	Pixelfarm_Import_Xlsx_Products_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pixelfarm_import_xlsx_products' );
register_deactivation_hook( __FILE__, 'deactivate_pixelfarm_import_xlsx_products' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pixelfarm-import-xlsx-products.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pixelfarm_import_xlsx_products() {

	$plugin = new Pixelfarm_Import_Xlsx_Products();
	$plugin->run();

}
run_pixelfarm_import_xlsx_products();

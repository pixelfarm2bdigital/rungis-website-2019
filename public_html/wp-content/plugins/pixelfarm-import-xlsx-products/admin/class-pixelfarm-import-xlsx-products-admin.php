<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.pixelfarm.nl
 * @since      1.0.0
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/admin
 * @author     Pixelfarm <getintouch@pixelfarm.nl>
 */
class Pixelfarm_Import_Xlsx_Products_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pixelfarm_Import_Xlsx_Products_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pixelfarm_Import_Xlsx_Products_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pixelfarm-import-xlsx-products-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pixelfarm_Import_Xlsx_Products_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pixelfarm_Import_Xlsx_Products_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pixelfarm-import-xlsx-products-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function add_admin_menu(){
		
		
	    add_submenu_page(
	        'edit.php?post_type=product',
	        __( 'Importeren producten XLSX', 'textdomain' ),
	        __( 'Importeren', 'textdomain' ),
	        'manage_options',
	        'import-products',
	        array($this, 'admin_import_page')
	    );
	}

	
	public function admin_import_page(){
		if($_POST){
			require plugin_dir_path( __FILE__ )."partials/pixelfarm-import-xlsx-products-admin-handle.php";
		}else{
			if(isset($_GET['fixcurrentimages']) && $_GET['fixcurrentimages'] == "1"){
				require plugin_dir_path( __FILE__ )."partials/pixelfarm-import-xlsx-products-admin-fix.php";
			}else{
				require plugin_dir_path( __FILE__ )."partials/pixelfarm-import-xlsx-products-admin-display.php";	
			}
			
		}
	}
	
}

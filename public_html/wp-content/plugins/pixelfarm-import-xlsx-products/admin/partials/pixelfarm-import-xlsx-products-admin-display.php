<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.pixelfarm.nl
 * @since      1.0.0
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/admin/partials
 */








/*
*
$products = get_posts(
	array(
		"post_type"=>"product",
		"posts_per_page"=>-1,
		"post_status"=>"any"
	)
);

foreach($products as $product){

		wp_delete_post($product->ID);
		
}
die();

/*
*/
ini_set('upload_max_filesize', '999M');     
ini_set('max_execution_time', '999');
ini_set('memory_limit', '1024M');
ini_set('post_max_size', '1000M'); 


if(isset($_GET['fixproducts'])){
	echo "<pre>";

	$allproducts = get_posts(
		array(
			"post_type"=>"product",
			"posts_per_page"=>-1,
			"order"=>"ASC"
		)
	);

	foreach($allproducts as $key=> $prod){
		$artnr = get_field("reference",$prod);

		$gallery = get_field("product_slider",$prod);
		
		foreach($gallery as $item){

			if(is_numeric($item)){
				$item2 = basename(get_attached_file($item));
				$imageid = $item;
			}else{
				$item2 = $item['filename'];
				$imageid = $item['id'];
			}
			if(!preg_match("/".$artnr."_\d+/", $item2)){
				continue;
			}else{
				set_post_thumbnail( $prod, $imageid);
				break;
			}

			die();
		}
		




	}
	







}


?>


<form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post" enctype="multipart/form-data">

	
	<h3>Nieuwe zip en XLSX uploaden</h3>
	<p>Selecteer bij het eerste veld de Excel lijst met producten. Deze zullen geupdate worden. Bij het 2e veld kunt je een zip uploaden met afbeeldingen. Deze worden toegevoegd als afbeeldingen aan de producten<a href='?post_type=product&page=import-products&fixcurrentimages=1'>.</a></p>
	
	<table class="form-table">
	    <tbody>
	        <tr>
	        	<td>
	        		<label>
						<input type='file' name='file_xlsx'>
						<span>XLSX</span>
					</label>
	        	</td>
	        </tr>
	        <tr>
	        	<td>
					<label>
						<input type='file' name='file_images'>
						<span>ZIP (images)</span>
					</label>
				</td>
	        </tr>
	        <tr>
	        	<td>
					<button name='submit' value='sum' class='button-primary' type='submit'>Uploaden</button>        		
	        	</td>
	        </tr>
	    </tbody>
	</table>
	
	
	



</form>
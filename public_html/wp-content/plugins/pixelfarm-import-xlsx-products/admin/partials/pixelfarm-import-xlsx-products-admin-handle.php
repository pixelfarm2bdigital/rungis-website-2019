<?php

ini_set('upload_max_filesize', '999M');     
ini_set('max_execution_time', '999');
ini_set('memory_limit', '2048M');
ini_set('post_max_size', '1000M'); 


function array_combine2($arr1, $arr2) {
    $count = min(count($arr1), count($arr2));
    return array_combine(array_slice($arr1, 0, $count), array_slice($arr2, 0, $count));
}

if($_FILES['file_xlsx']['tmp_name']){

	$reader = new SpreadsheetReader($_FILES['file_xlsx']['tmp_name'],"Products.xlsx");

	$rows = array();
	foreach ($reader as $Row){
	    $rows[] = $Row;
	}




	$info = array_shift($rows);
	
	
	
	$headers = $info;
	

	foreach($rows as $key=> $row){
		foreach($row as $rk => $rv){
			$row[$rk] = trim($rv);
		}
		if($key > 10){
			//break;
		}
		$product = array_combine2($headers, $row);
		
	
			

		if(trim($product['Product Name']) == ""){
			continue;
		}

		

		$newStatus = (strtolower($product['Complete']) == "yes"?"publish":"draft");
		$biologisch = (@$product['Tags'] == "Biologisch" || @$product['Biologisch'] == "Ja" || @$product['Biologisch'] == "Yes"?true:false);


		
		//find product by product id
		$args = array(
				"post_type"		=> "product",
				"meta_key"		=> "reference",
				"meta_value"	=> $product['Reference']."",
				"posts_per_page"=> 1,
				"post_status"	=> "any"

			);
		/*
	'posts_per_page'   => 5,
		'offset'           => 0,
		'category'         => '',
		'category_name'    => '',
		'orderby'          => 'date',
		'order'            => 'DESC',
		'include'          => '',
		'exclude'          => '',
		'meta_key'         => '',
		'meta_value'       => '',
		'post_type'        => 'post',
		'post_mime_type'   => '',
		'post_parent'      => '',
		'author'	   => '',
		'author_name'	   => '',
		'post_status'      => 'publish',
		'suppress_filters' => true 
		*/
		$posts = get_posts(
			$args
		);


		if(empty($posts)){

			//create a new product
			$post_data = array(
				'post_title'    => $product['Product Name'],
				'post_content'  => "",
				'post_status'   => $newStatus,
				'post_type'     => 'product',
				'post_author'   => 1,
			);

			$postID = wp_insert_post( $post_data, $error_obj );
			
		}else{
			$postID = $posts[0]->ID;
		}

		

		if($postID){
			//update the fields

			$my_post = array(
				'ID'			=>	$postID,
				'post_title'	=>	$product['Product Name'],
			);

			// Update the post into the database
			wp_update_post( $my_post );

			update_field("reference", $product['Reference'], $postID);
			update_field("product_intro_text", $product['Short Description'], $postID);
			update_field("product_meerinformatie", $product['Long Description'], $postID);
			update_field("top_sellers", $product['Top sellers'], $postID);
			update_field("biologisch", $biologisch, $postID);
			update_field("can_be_ordered", $product['Can be ordered'], $postID);
			update_field("land_of_origin", $product['Land of Origin'], $postID);
			



			//Lente	Voorzomer	Zomer	Herfst	Winter
			update_field("winter",(strtolower($product['Winter'])=="yes"),$postID);
			update_field("spring",(strtolower($product['Lente'])=="yes"),$postID);
			update_field("presummer",(strtolower($product['Voorzomer'])=="yes"),$postID);
			update_field("summer",(strtolower($product['Zomer'])=="yes"),$postID);
			update_field("autumn",(strtolower($product['Herfst'])=="yes"),$postID);



			update_field("bestelbaar_zakelijk",(strtolower(@$product['Bestel zakelijk'])=="yes"),$postID);
			update_field("bestelbaar_particulier",(strtolower(@$product['Bestel particulier'])=="yes"),$postID);

			
			$terms = array();



			//get the taxonomy, already exists?
			$term_id = term_exists( $product['Category'], "productcategory");
			if(!$term_id){
				//insert new term
				$parent_term = term_exists( 'fruits', 'product' ); // array is returned if taxonomy is given
				$parent_term_id = $parent_term['term_id']; // get numeric term id
				$term = wp_insert_term(
					$product['Category'], // the term 
					'productcategory' // the taxonomy
					
				);
				$term_id = $term['term_id'];


			}else{
				$term_id = $term_id['term_id'];
			}
			if($term_id){

				$terms[] = intval($term_id);

				

				if(isset($product['Sub category']) && $product['Sub category']){
					$subterm_id = term_exists( $product['Sub category'], "productcategory",$term_id);
					
					if(!$subterm_id){
						$subterm = wp_insert_term(
							$product['Sub category'], // the term 
							'productcategory',
							array(
							    'parent'=> $term_id
							)
						);
						$subterm_id = $subterm['term_id'];
						
					}else{
						$subterm_id = $subterm_id['term_id'];
					}

					if($subterm_id){
						$terms[] = intval($subterm_id);
					}
					

					


				}



				wp_set_object_terms( $postID, $terms , 'productcategory', false);	

				




			}
			
		}
		

		/*

	[Photo's changed] => 
	[Complete] => Yes
	[Can be ordered] => Yes
	[Reference] => 270335
	[Category] => Groenten
	[Sub category] => Vrucht groenten
	[Product Name] => Biologische cherry tomaat mix
1	[Short Description] => Biologische cherry tomaten mix van Frank de Koning. Frank de Koning teelt de lekkerste tomatenrassen. En zo is ook deze cherry mix eruit gekomen. Een heerlijke smaakvolle en 	kleurrijke mix.
1	[Long Description] => Ecoville, de biologische specialist van Rungis, is dé expert in biologische AGF. Ecoville levert via Rungis jaarrond een compleet assortiment aan aardappelen, groenten, fruit en 	verse kruiden en is SKAL gecertificeerd. Van basisproducten tot exclusieve producten, want praktisch alle AGF is biologisch beschikbaar. Ecoville werkt nauw samen met biologische boeren, zorgboerderijen 	en tuinders uit de buurt om alle producten kraakvers bij u in de keuken te krijgen.
	[Price] => € 1.95
	[Unit] => 200 gram
	[Product Weight] => 200 gram
	[Land of Origin] => Nederland, Tinte
	[Condition] => Koelvers
	[Tags] => Biologisch
	[Tags2] => 
	[Top sellers] => 30
	[] => 

*/
		//print_r($product);
	}
}


if($_FILES['file_images']['tmp_name']){

	$edittedPosts = array();
	$zip = zip_open($_FILES['file_images']['tmp_name']);
	
	if ($zip) {
		while ($zip_entry = zip_read($zip)) {
			$filename = basename(zip_entry_name($zip_entry));
			$parts = explode("_", $filename);
			if(count($parts) > 1){




				if (zip_entry_open($zip, $zip_entry,"r")){
					
					file_put_contents(WP_CONTENT_DIR."/uploads/products/".$filename,zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
					//die();

					//$upload = wp_upload_bits($filename, null, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)));
				
					
					


				
					
					zip_entry_close($zip_entry);
				}

				
			}
		}

		zip_close($zip);
	}

	echo "<h3>Upload complete</h3>";

}


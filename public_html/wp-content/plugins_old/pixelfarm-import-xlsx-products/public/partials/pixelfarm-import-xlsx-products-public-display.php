<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.pixelfarm.nl
 * @since      1.0.0
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.pixelfarm.nl
 * @since      1.0.0
 *
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Pixelfarm_Import_Xlsx_Products
 * @subpackage Pixelfarm_Import_Xlsx_Products/includes
 * @author     Pixelfarm <getintouch@pixelfarm.nl>
 */
class Pixelfarm_Import_Xlsx_Products_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'pixelfarm-import-xlsx-products',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

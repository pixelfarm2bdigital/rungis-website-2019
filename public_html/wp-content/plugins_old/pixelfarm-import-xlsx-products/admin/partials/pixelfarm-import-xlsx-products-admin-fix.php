<?php


//find all currently used images in products get the actual image path so we can move it


//10009_2

ini_set('upload_max_filesize', '999M');     
ini_set('max_execution_time', '999');
ini_set('memory_limit', '1024M');
ini_set('post_max_size', '1000M'); 
echo "<pre>";
$imageProducts = array();
$products = get_posts(
	array(
		"post_type"=>"product",
		"posts_per_page"=>-1,
		"order"=>"ASC"
	)
);

$imageids = array();

if(ICL_LANGUAGE_NAME != ''){
	die("not the right language".ICL_LANGUAGE_NAME);
}
$imagepath = wp_get_upload_dir();

$imagepath = $imagepath['basedir'];


foreach($products as $prod){
	$artnr = get_field("reference",$prod);

	$gallery = get_field("product_slider",$prod);
	
	if(!$gallery) $gallery = array();

	
	$imageProducts[$artnr] = array();

	foreach($gallery as $item){



		
		if(is_numeric($item)){

			$fullpath = get_attached_file($item);
			$imageid = $item;
		}else{
			$fullpath = get_attached_file($item['id']);
			$imageid = $item['id'];
		}
		
		$basename = basename($fullpath);


		if(!preg_match("/".$artnr."_\d+/", $basename)){
			continue;			
		}



		$basename = explode("-", $basename);
		if(count($basename) == 1){
			$newname = $basename[0];
		}else{
			$newname = $basename[0].".jpg";
		}




		$dest = $imagepath."/products/".$newname;
		


		

		copy($fullpath,$dest);

		$imageids[] = $imageid;

		wp_delete_attachment($imageid,true);

		
	}



	update_field("product_slider",array(),$prod);
	delete_post_thumbnail( $prod );

}

print_r($imageids);

die();